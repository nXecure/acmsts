antiX Community Multilanguage Support Test Suite (aCMSTS)
Ver 0.37
------------------------------------------------------------------------------------------------------------------------------------------
DE: Maschinelle Übersetzungen ins Französische, Englische, Portugiesische und Russische befinden sich weiter unten in diesem Dokument.

FR: Des traductions automatiques en français, anglais, portugais et russe sont fournies plus loin dans ce document.

EN: Machine translations into French, English, Portuguese and Russian are provided later in this document.

pt_BR: As traduções automáticas para francês, inglês, português e russo são fornecidas mais adiante neste documento.

RU: Машинный перевод на французский, английский, португальский и русский языки приводится ниже в этом документе.
------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------------------------
Deutsch (Originaltext)
------------------------------------------------------------------------------------------------------------------------------------------

Beschreibung:
Das Script “antiX Community Multilanguage Support Test Suite (aCMSTS)” dient der Korrektur und dem sofortigen Test von Sprachdateien in antiX. Es ist so konzipiert, daß unerfahrene Nutzer von antiX, die aber über gute Sprachkenntnisse verfügen, auf einfache Art Korrekturen an Übersetzungen von Programmoberflächen vornehmen und auprobieren können.
Sowohl sprachspezifische Dateien (aus Verzeichnissen wie z.B. fr, de, ru für Französisch, Deutsch, Russisch...) als auch landesspezifische Sprachdateien (aus Verzeichnissen wie beispielsweise pt_BR, pt_PT, de_DE, de_CH, de_AT für Brasilianisches und Portugiesisches Portugiesisch, oder Deutsches, Schweizerisches und Österreichisches Deutsch ...) können bearbeitet werden. Nicht alle Programme haben Sprachdateien in allen Sprachen. Wird eine gesuchte landesspezifische Sprachdatei (z.B. "fr_CA" für Kanadisches Französich) nicht gefunden, empfiehlt es sich, die entsprechende Datei im nur sprachspezifischen Verzeichnis (z.B. "fr" = Französisch universell) zu suchen, die vom betreffenden Programm in diesem Fall auch verwendet wird. Außerdem ist es möglich, neue Übersetzungen zu einem Programm hinzuzufügen und zu testen, bevor sie bei transifex eingereicht werden.

Die zugehörigen Übersetzungen der Programmeinträge im antiX Menu können auch hinzugefügt oder korrigiert werden.

Bitte erst Änderungen in transifex vornehmen, nachdem sie mit diesem Script auf Funktionsfähigkeit überprüft worden sind. Ungetestete Änderungen können (und haben dies in der Vergangenheit bereits) zu Fehlfunktionen bei Ausführung der Programme auf Systemen in in der betroffenen Sprache führen. Bei Problemen mit selbst korrigierten Sprachdateien bitte einfach im antiX-Forum anfragen.



Installation:

Nach dem Entpacken das Script "installer.sh" im Terminal-Fenster ausführen (evtl. "/bin/bash installer.sh eingeben"). Bitte achten Sie darauf, daß am Ende der Installation "Installation has completed successfully." im Terminalfenster angezeigt wird.



Anleitung:

Die Testsuite wird mit dem Eintrag "aCMSTS" im antiX-Menü gestartet.
Bitte exakt den Anweisungen in den Programmfenstern folgen. Alle notwendigen Schritte werden vom Script automatisch in der richtigen Abfolge ausgeführt. Besonders wichtig ist es, auf die "Speichern"-Schaltfläche im "poedit"-Fenster zu klicken, bevor das poedit-Fenster geschlossen wird. Anderenfalls kann das Script nicht korrekt zuende geführt werden.

Jede Sprachdatei jedes Programmes in jeder Sprache, die auf dem System in einem der Verzeichnisse /usr/share/locale/x/LC_MESSAGES vorhanden ist, kann korrigiert und getestet werden. Wenn alles wie erwartet funktioniert, können die Korrekturen in transifex vorgenommen werden.





------------------------------------------------------------------------------------------------------------------------------------------
Automatische Übersetzungen:
------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------------------------
English (machine-translated)
------------------------------------------------------------------------------------------------------------------------------------------


Description:
The script “antiX Community Multilanguage Support Test Suite (aCMSTS)” is used to correct and immediately test language files in antiX. It is designed in such a way that inexperienced users of antiX who have good language skills can easily make corrections to translations of programme interfaces.
Both language-specific files (from directories such as fr, de, ru for French, German, Russian...) and country-specific language files (from directories such as pt_BR, pt_PT, de_DE, de_CH, de_AT for Brazilian and Portuguese Portuguese, or German, Swiss and Austrian German...) can be edited. Not all programmes have language files in all languages. If the one country-specific language file you are looking for (e.g. "fr_CA" for Canadian French) is not found, it is advisable to look for the corresponding file in the only language-specific directory (e.g. "fr" = French universal), which is also used by the programme in question in this case. It is also possible to add new translations to a program and test them before submitting them to transifex. The corresponding translations of the programme entries in the antiX menu can also be added or corrected.

Please only make changes in transifex after they have been tested for functionality with this script. Untested changes can (and have in the past) lead to malfunctions when running the programmes on systems in the affected language. If you have problems with self-corrected language files, please just ask in the antiX forum.



Installing the program:
After unpacking, execute the script "installer.sh" in a terminal window (possibly enter "/bin/bash installer.sh"). Please make sure that "Installation has completed successfully" is displayed in the terminal window at the end of installation process.



Instructions:

Chose the menuentry "aCMSTS" from antiX menu.

Please follow the instructions in the programme windows exactly. All necessary steps are then automatically executed by the script in the correct sequence. It is especially important to click on the "Save" button in the "poedit" window before closing the poedit window. Otherwise, the script cannot be completed correctly.

Every language file of every programme in every language that exists on the system in one of the directories /usr/share/locale/x/LC_MESSAGES can be corrected and tested. If everything works as expected, the corrections can be made in transifex.





------------------------------------------------------------------------------------------------------------------------------------------
Français (traduction automatique)
------------------------------------------------------------------------------------------------------------------------------------------

Description :
Le script “antiX Community Multilanguage Support Test Suite (aCMSTS)” est utilisé pour corriger et tester immédiatement les fichiers de langue dans antiX. Il est conçu de telle manière que les utilisateurs inexpérimentés d'antiX, qui ont de bonnes connaissances linguistiques, peuvent facilement apporter des corrections aux traductions des interfaces des programmes.
Il est possible d'éditer à la fois des fichiers de langue spécifiques (provenant de répertoires comme fr, de, ru pour le français, l'allemand, le russe...) et des fichiers de langue spécifiques à un pays (provenant de répertoires comme pt_BR, pt_PT, de_DE, de_CH, de_AT pour le portugais brésilien et portugais, ou l'allemand, le suisse et l'autrichien...). Tous les programmes ne disposent pas de fichiers de langue dans toutes les langues. Si un fichier de langue spécifique à un pays (par exemple "fr_CA" pour le français canadien) n'est pas trouvé, il est recommandé de rechercher le fichier correspondant dans le seul répertoire spécifique à la langue (par exemple "fr" = français universel), qui est également utilisé par le programme respectif dans ce cas. Il est également possible d'ajouter de nouvelles traductions à un programme et de les tester avant de les soumettre à transifex. Les traductions correspondantes des entrées du programme dans le menu antiX peuvent également être ajoutées ou corrigées.

Veuillez ne pas effectuer de modifications dans transifex avant d'avoir testé la fonctionnalité de ce script. Des modifications non testées peuvent (et ont par le passé) entraîner des dysfonctionnements lors de l'exécution des programmes sur les systèmes dans la langue concernée. Si vous avez des problèmes avec les fichiers de langue auto-corrigés, veuillez simplement demander dans le forum antiX.



Installation du programme:
Après le déballage, exécutez le script "installer.sh" dans une fenêtre de terminal (éventuellement entrer dans "/bin/bash installer.sh"). Veuillez vous assurer qu'à la fin de l'installation "Installation has completed successfully." (L'installation s'est terminée avec succès). s'affiche dans la fenêtre du terminal.



Instructions :

La suite de tests est lancée avec l'entrée "aCMSTS" dans le menu antiX.

Veuillez suivre exactement les instructions dans les fenêtres du programme. Toutes les étapes nécessaires seront alors exécutées automatiquement par le script dans l'ordre correct. Il est particulièrement important de cliquer sur le bouton "Enregistrer" dans la fenêtre "poedit" avant de fermer la fenêtre "poedit". Dans le cas contraire, le scénario ne peut pas être complété correctement.

Tout fichier de langue d'un programme dans une langue quelconque présent sur le système dans l'un des répertoires /usr/share/locale/x/LC_MESSAGES peut être corrigé et testé. Si tout fonctionne comme prévu, les corrections peuvent être effectuées en transifex.





------------------------------------------------------------------------------------------------------------------------------------------
Português do Brasil (adaptação da tradução automática por marcelocripe no dia 06-02-2021)
------------------------------------------------------------------------------------------------------------------------------------------

Descrição:
O script “antiX Community Multilanguage Support Test Suite (aCMSTS)” (Conjunto de Teste de Suporte Multilíngüe da Comunidade AntiX) é usado para corrigir e testar imediatamente os arquivos de tradução de idioma no antiX. Ele é projetado de forma que usuários inexperientes do antiX, que possuem bons conhecimentos em tradução de línguas e idiomas, possam facilmente fazer as correções nas traduções das interfaces dos programas e testá-las.
Tanto em arquivos de idiomas específicos (de diretórios como fr, de, ru para o francês, alemão, russo...) quanto em arquivos de idiomas de países específicos (de diretórios como pt_BR, pt_PT, de_DE, de_CH, de_AT para português do Brasil e português de Portugal, ou alemão, suíço e austríaco alemão...) podem ser editados. Nem todos os programas possuem arquivos de idioma em todos os idiomas. Se um arquivo de idioma específico do país que você está procurando (por exemplo, "fr_CA" para o francês canadense) não for encontrado, recomenda-se pesquisar o arquivo correspondente no diretório específico do idioma (por exemplo, "fr" = francês universal), que também é utilizado pelo programa em questão. Também é possível acrescentar novas traduções a um programa e testá-las antes de submetê-las à transifex. As traduções correspondentes das entradas do programa no menu antiX também podem ser adicionadas ou corrigidas.

Por favor, não faça alterações ou edições diretamente no sítio/site transifex sem antes ter sido testadas as funcionalidades com este script. As alterações ou edições não testadas podem levar (e já levaram) a mau funcionamento ao executar os programas em sistemas no idioma em questão. Se você tiver problemas com os arquivos de idioma corrigidos por você mesmo, por favor, pergunte no fórum do antiX.



Instalando o programa:

Após descompactar, execute o script "installer.sh" em uma janela de terminal. (possivelmente digite "/bin/bash installer.sh"ou "./installer.sh").  Por favor, certifique-se de que ao final da instalação "Installation has completed successfully." (Instalação foi concluída com sucesso.) é exibido na janela do terminal.



Instruções:

O conjunto de testes é iniciado com a entrada "aCMSTS" no menu antiX.

Por favor, siga exatamente as instruções nas janelas do programa. Todas as etapas necessárias são executadas automaticamente pelo script na sequência correta. É extremamente importante clicar no botão "Salvar" na janela do "poedit" antes de fechar. Caso contrário, o script não pode ser concluído corretamente.

Qualquer arquivo de idioma de qualquer programa em qualquer idioma disponível no sistema em qualquer um dos diretórios /usr/share/locale/x/LC_MESSAGES pode ser corrigido e testado. Se tudo funcionar conforme o esperado, as correções podem ser feitas no sítio/site transifex.





------------------------------------------------------------------------------------------------------------------------------------------
Русский язык (машинный перевод)
------------------------------------------------------------------------------------------------------------------------------------------

Описание:
Скрипт “antiX Community Multilanguage Support Test Suite (aCMSTS)” используется для исправления и немедленного тестирования языковых файлов в antiX (антиКС). Она сконструирована таким образом, что неопытные пользователи antiX, обладающие хорошими языковыми навыками, могут легко вносить исправления в переводы программных интерфейсов.
Можно редактировать как языковые файлы (из каталогов типа fr, de, ru для французского, немецкого, русского...), так и языковые файлы для конкретной страны (из каталогов типа pt_BR, pt_PT, de_DE, de_CH, de_AT для бразильского и португальского португальского, или немецкого, швейцарского и австрийского немецкого...). Не все программы имеют языковые файлы на всех языках. Если языковой файл по конкретной стране (например, "fr_CA" для канадского французского) не найден, рекомендуется искать соответствующий файл в единственном каталоге по конкретному языку (например, "fr" = французский универсальный), который в данном случае также используется соответствующей программой. Также возможно добавить новые переводы в программу и протестировать их перед отправкой на Transifex (трансифекс). Также могут быть добавлены или исправлены соответствующие переводы программных записей в меню antiX.

Пожалуйста, вносите изменения в Transifex только после того, как они будут протестированы на функциональность с помощью этого скрипта. Непроверенные изменения могут (и в прошлом) привести к сбоям при запуске программ на системах на соответствующем языке. Если у вас возникли проблемы с самокорректирующимися языковыми файлами, пожалуйста, просто спросите в форуме antiX.



Установка программы:
После распаковки выполните скрипт "installer.sh" в окне терминала. (возможно, введите "/bin/bash installer.sh").
Пожалуйста, убедитесь, что по окончании установки "Installation has completed successfully." (Установка успешно завершена.) отображается в окне терминала.



Инструкции:

Испытательный комплект запускается с помощью пункта "aCMSTS" в меню antiX.

Пожалуйста, точно следуйте инструкциям в окнах программы. После этого все необходимые шаги будут автоматически выполнены скриптом в правильной последовательности. Особенно важно нажать кнопку "Сохранить" в окне "poedit" перед закрытием окна "poedit". В противном случае скрипт не может быть корректно завершен.

Любой языковой файл любой программы на любом языке, присутствующем в системе в любом из каталогов /usr/share/locale/x/LC_MESSAGES может быть исправлен и протестирован. Если все работает, как ожидается, поправки могут быть сделаны в Transifexе.

------------------------------------------------------------------------------------------------------------------------------------------
