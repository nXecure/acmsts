#! /bin/bash
# antiX Community Multilanguage Support Test Suite (aCMSTS)
# written for antiX Community by Robin.antiX Jan-Feb.2021
# tested on antiX 19.3 by marcelocripe Feb-Mar 2021
# Please respect licence information at bottom of this script.
# Ver. 0.37a

VERSION="0.37a"

# todo: add section to enable user to add and test a translation for languages not present still, so he can work and test on it before transferring the result to transifex.
# todo: write usage information and help output -h /--help
# todo: make script fully compatible to country specific language IDs. For now it uses two letter code to establish testing environment, but for chosing, editig and saving .po and .mo files full country specific support is utilised already.
# todo: handling of errors, error management. For now we'll exit always with err status zero.
# todo: make handling of working directory a bit more smoth for user.

# Section 1: Prerequisites
# ========================

TEXTDOMAINDIR=/usr/share/locale 
TEXTDOMAIN=antiX-multilanguage-test-suite.sh

# command switches
SWITCH_DEBUG=0										# set to 0 for normal operation, 1 for printing internal information to terminal while running.
SWITCH_EXPERIENCED_USER=0							# set to 0 for normal operation. Setting to 1 saves experienced users some extra clicks, presuming safe standard values
SWITCH_FUNCTION=0
SWITCH_UNDECORATED=0								# not implemented function as yet. set to 0 for normal operation. Seting to 1 displayes user dialogs in yad windows undecorated. Has no influence on external programs used by script.

# set some flags for logical structure
FLAG_STATE=255										# (has to be set to 255) initial value for logical evaluation structure, mangement of back/forward movement through program steps.
FLAG_DONE=0										    # (has to be set to 0 (zero)) initial value for logical evaluation structure 
FLAG_ABORT=0										# (has to be set to 0 (zero)) initial value for logical evaluation structure 
FLAG_MULTIPLE=0										# (has to be set to 0 (zero)) initial value for checking whether multiple instances of this test suite are running
FLAG_antiX_LANG_DIR=0								# (has to be set to 0 (zero)) initial value for determining whether we have created an own virtual language file directory in this instance when running multiple instances

# UI-Language and antiX virtual test language name
UI_LANG="$(echo $LANG | cut -c -2)"						# get interface language user utilises. For now use only first two letters of specificator
#UI_LANG_4="$(echo $LANG | cut -c -5)"					# not in use for now. Same as "$UI_LANG", but makes use of 4 significant digits for country specific language identification (ll_CC)
#UI_LANG_FULL="$LANG"									# not in use for now. Makes use of complete Language ID, as e.g. "zh_CN.GB2312" or "de@hebrew"
antiX_TEST_LANG="antiX_test"							# set name for antiX "virtual" test language

# Set some Path variables to initial values
LANG_BASE_DIR="/usr/share/locale/"						# system folder containing language subfolder structure
LANG_MSG_DIR="/LC_MESSAGES/"							# subfolder for message strings
TMP_LANG_DIRS="/tmp/aCMSTS_$$01.TMP"					# file will contain a list of available languages (=directories)
TMP_SH_FILE="/tmp/aCMSTS_$$03.sh"						# file will contain command for a workaround solution for switching to appropriate language in test window
TMP_LANG_DESK="/tmp/aCMSTS_$$04.TMP"					# file will contain a list of languages found in .desktop file
TMP_DESKTOP_FILE="/tmp/aCMSTS_$$05.TMP"					# file will contain temporary copy of .desktop file in test
UI_LANG_DIR="$LANG_BASE_DIR$UI_LANG$LANG_MSG_DIR"		# corelate user interaface language to language-file-directory testing environment will engage.
antiX_TEST_LANG_DIR="$LANG_BASE_DIR$antiX_TEST_LANG$LANG_MSG_DIR"		#directory for virtual antiX_test language in system language folder structure
WORK_DIR="$HOME/aCMSTS translations"					# define standard working directory
LOCKFILE="/tmp/aCMSTS.lock"								# set standard lockfile
IMG_DIR="/usr/share/icons/hicolor/scalable/apps"		# icon directory
FOLDERLIST="/usr/share/applications,/usr/share/applications/antix,/home/demo/.local/share/applications,/home/demo/.local/share/applications/TCM"	# comma seperated list for user choice folders of known places possibly containing .desktop files
DESK_ENTRY_DIR="/usr/share/applications"				# initial value for directory to search for .desktop files in
REPORTFILE="\$WORK_DIR_SESSION/aCMSTS-report.txt"		# set filename for report file to be created in $WORK_DIR. You may add a path to it for preselecting any other location. If you don't want to get a report at all, please set this value to "/dev/null" rather.

# Set Window title and icons (yad)
TXT_TITLE=$"antiX Community Multilanguage Test Suite"
# TXT_TITLE=$"antiX Language File Test Tool"
IMG_ICON="$IMG_DIR/aCMSTS-improve-translation.svg"
IMG_ICON_aCMSTS="$IMG_DIR/aCMSTS-improve-translation.svg"
IMG_ICON_ERROR="$IMG_DIR/aCMSTS-warning.svg"
IMG_ICON_ASK="$IMG_DIR/aCMSTS-question.svg"
IMG_ICON_LANG="$IMG_DIR/aCMSTS-start-translation.png"
IMG_ICON_APP="$IMG_DIR/aCMSTS-applications.svg"
IMG_ICON_DECOMPILE="$IMG_DIR/aCMSTS-decompile.png"
IMG_ICON_STOP="$IMG_DIR/aCMSTS-stop.svg"
IMG_ICON_TEST="$IMG_DIR/aCMSTS-test-language-file.svg"
IMG_ICON_SAVE="$IMG_DIR/aCMSTS-copy-mo-back.png"
IMG_ICON_MENU="$IMG_DIR/aCMSTS-antiX-menu.png"
DECORATIONS_BORDER=""
DECOR_ICONS="--image="

# Set Error handling codes
ERR_NO=0																# initial value for exitcode

# some plain text output in terminal in case graphical X windows wouldn't display any dialogs
echo -en "\n   === $TXT_TITLE, Version $VERSION. ===\n"

# test for some special helper programs needed to be present on user's system
FLAG_MISSING=0
if [ ! $(command -v poedit) ]; then TXT_ERROR_MISSING=$"\nˮpoeditˮ is not installed.\n   --> Please install ˮpoeditˮ before executing this script.\n" && FLAG_MISSING=1; fi
if [ ! $(command -v msgunfmt) ]; then TXT_ERROR_MISSING=$TXT_ERROR_MISSING$"\nˮmsgunfmtˮ command is not found.\n   --> Please install ˮgettextˮ before executing this script.\n" && FLAG_MISSING=1; fi
# if [ ! $(command -v lxterminal) ] || [ ! $(command -v roxterm) ]; then TXT_ERROR_MISSING=$TXT_ERROR_MISSING$"\n No compatible Terminal Emulator found.\n   --> Please make sure either ˮLXterminalˮ\n\tor ˮROXtermˮ is installed on your\n\tsystem before executing this script.\n" && FLAG_MISSING=1; fi
if [ ! $(command -v lxterminal) ]; then if [ ! $(command -v roxterm) ]; then TXT_ERROR_MISSING=$TXT_ERROR_MISSING$"\n No compatible Terminal Emulator found.\n   --> Please make sure either ˮLXterminalˮ\n\tor ˮROXtermˮ is installed on your\n\tsystem before executing this script.\n" && FLAG_MISSING=1; fi; fi			# bugfix for antiX 19.3
if [ ! $(command -v yad) ]; then TXT_ERROR_MISSING=$TXT_ERROR_MISSING$"\nˮyadˮ is not installed.\n   --> Please install ˮyadˮ before executing this script.\n" && FLAG_MISSING=1 ; fi
if [ ! $(command -v gksudo) ]; then TXT_ERROR_MISSING=$TXT_ERROR_MISSING$"\nˮgksudoˮ is not installed.\n   --> Please install ˮgksudoˮ before executing this script.\n" && FLAG_MISSING=1 ; fi
if [ ! $(command -v sed) ]; then TXT_ERROR_MISSING=$TXT_ERROR_MISSING$"\nˮsedˮ is not installed.\n   --> Please install ˮsedˮ before executing this script.\n" && FLAG_MISSING=1 ; fi
if [ ! $(command -v awk) ]; then TXT_ERROR_MISSING=$TXT_ERROR_MISSING$"\nˮawkˮ is not installed.\n   --> Please make sure either ˮawkˮ or ˮgawkˮ is installed\n\ton your system before executing this script.\n" && FLAG_MISSING=1 ; fi
if [ ! $(command -v bash) ]; then TXT_ERROR_MISSING=$TXT_ERROR_MISSING$"\nˮbashˮ is not installed.\n   --> Please make sure true ˮbashˮ is available on your\n\tsystem before executing this script.\n" && FLAG_MISSING=1 ; fi
if [ ! $(command -v wmctrl) ]; then TXT_ERROR_MISSING=$TXT_ERROR_MISSING$"\nˮwmctrlˮ is not installed.\n   --> Please install ˮwmctrlˮ before executing this script.\n" && FLAG_MISSING=1 ; fi
if [ ! $(command -v grep) ]; then TXT_ERROR_MISSING=$TXT_ERROR_MISSING$"\nˮgrepˮ is not installed.\n   --> Please install ˮgrepˮ before executing this script.\n" && FLAG_MISSING=1 ; fi
if [ ! $(command -v pgrep) ]; then TXT_ERROR_MISSING=$TXT_ERROR_MISSING$"\nˮpgrepˮ is not installed.\n   --> Please install ˮpgrepˮ before executing this script.\n" && FLAG_MISSING=1 ; fi

if [ $FLAG_MISSING == 1 ]; then
	echo "\n$TXT_ERROR_MISSING\n"
	yad --center $DECOR_ICONS$IMG_ICON_ERROR $DECORATIONS \
	    --title="$TXT_TITLE" \
		--window-icon="$IMG_ICON" \
	    --text="$TXT_ERROR_MISSING" \
	    --button="OK"
	exit 1
fi

# get user permission from for writing in system folders. We have to prevent user from skipping a first command and accepting a second only while executing multiple commands successively.
function get_permissions {
		SUDO_PERMISSION=0
		while [ $SUDO_PERMISSION == 0 ]
			do
				gksudo -- whoami > /dev/null
				if [ $? == 0 ]; then
					SUDO_PERMISSION=1
				else
					echo -en "\nCan't proceed due to lack of permissions\n ...retry\n"
				fi
			done
		# todo: inform user about problem with permissions, and make him decide whether to try again or abort.
}

# prepare cleanup on exit
function cleanup {
if [ -e "$LANG_BASE_DIR$antiX_TEST_LANG" ] && [ $FLAG_antiX_LANG_DIR == 1 ]; then   # has to be checked (by flag) if it was created in this instance before deleting it on exit in early program stage. Otherwise we might kill the directory created by another instance.
	get_permissions
	gksudo -- rm -rf "$LANG_BASE_DIR$antiX_TEST_LANG"
fi
[ -e "$LOCKFILE" ] && rm -rf "$LOCKFILE"								# was created discriminable in each instance of antiX language file test suite so we can safely delete it.
[ -e "$TMP_LANG_DIRS" ] && rm -rf "$TMP_LANG_DIRS"						# was generated with random apendix (proc-ID)
[ -e "$TMP_LANG_FILES" ] && rm -rf "$TMP_LANG_FILES"					# was generated with random apendix (proc-ID)
[ -e "$TMP_SH_FILE" ] && rm -rf "$TMP_SH_FILE"							# was generated with random apendix (proc-ID)
[ -e "$TMP_LANG_DESK" ] && rm -rf "$TMP_LANG_DESK"						# was generated with random apendix (proc-ID)
[ -e "$TMP_DESKTOP_FILE" ] && rm -rf "$TMP_DESKTOP_FILE"					
if [ -e "$WORK_DIR_SESSION" ]; then [ -z "$(find "$WORK_DIR_SESSION" -type f)" ] && rm -rf "$WORK_DIR_SESSION"; fi  # remove session workdir if no files have been created
[ $SWITCH_DEBUG == 1 ] && echo "cleanup done"
[ $SWITCH_FUNCTION != 3 ] && echo -en "\n\tMischief managed. Leaving...\n"
return 0																# make sure this function returns success after execution even if there was nothing to do so last command (test) herein will come up with different exit code.
}

function reset-workdir {
[ -e "$SELECTED_APPLICATION.po" ] && rm -rf "$SELECTED_APPLICATION.po"
[ -e "$SELECTED_APPLICATION.mo" ] && rm -rf "$SELECTED_APPLICATION.mo"
[ -e "$SELECTED_APPLICATION.pot" ] && rm -rf "$SELECTED_APPLICATION.pot"
[ -e "$SELECTED_APPLICATION.mo.original" ] && rm -rf "$SELECTED_APPLICATION.mo.original"
[ -e "$REPORTFILE" ] && rm -rf "$REPORTFILE"
return 0
}

function generate-report {
echo -en $"$TXT_TITLE, Version $VERSION\n\n" >"$REPORTFILE"
echo -en "$TXT_REPORT_32$TXT_REPORT_40$TXT_REPORT_50$TXT_REPORT_60\n" >>"$REPORTFILE"
echo -en "Date of modification: `date`\n" >>"$REPORTFILE"
}

# trap 'cleanup && exit $ERR_NO' EXIT SIGQUIT SIGINT SIGSTOP SIGTERM ERR	# make sure we'll do cleanup on any way of exiting. #  Doesn't work with yad, will catch button events also!
trap 'cleanup && exit $ERR_NO' SIGINT

# evaluate command line options:
while getopts :epmhdu: FLAG_USER_INPUT									# set new or modify predefined variables according to user input
	do
	    case "${FLAG_USER_INPUT}" in
	        e)	SWITCH_EXPERIENCED_USER=1;;								# experienced user, (only in connection with -m or -p), no explanations and no extra choices displayed.
	        p)	SWITCH_FUNCTION=1;;										# edit type: program translation files (only in connection with -e)
	        m)	SWITCH_FUNCTION=2;;										# edit type: menu translation files (only in connection with -e)
	        h)	SWITCH_FUNCTION=3;;										# displays help for this test suite
	        d)	SWITCH_DEBUG=1;;										# prints some debug information when started in terminal window
	        u)	SWITCH_UNDECORATED=${OPTARG}							# uses "undecorated" yad windows
				case $SWITCH_UNDECORATED in
	        	1)	DECORATIONS="--undecorated";;
				2)	DECOR_ICONS=""										# uses (yad) windows without icons
					IMG_ICON_aCMSTS=""
					IMG_ICON_ERROR=""
					IMG_ICON_ASK=""
					IMG_ICON_LANG=""
					IMG_ICON_APP=""
					IMG_ICON_DECOMPILE=""
					IMG_ICON_STOP=""
					IMG_ICON_TEST=""
					IMG_ICON_SAVE=""
					IMG_ICON_MENU=""
				;;
				3)	DECORATIONS="--undecorated"							# uses (yad) windows with neither borders nor icons
					DECOR_ICONS=""
					IMG_ICON_aCMSTS=""
					IMG_ICON_ERROR=""
					IMG_ICON_ASK=""
					IMG_ICON_LANG=""
					IMG_ICON_APP=""
					IMG_ICON_DECOMPILE=""
					IMG_ICON_STOP=""
					IMG_ICON_TEST=""
					IMG_ICON_SAVE=""
					IMG_ICON_MENU=""
				;;
				*)	echo -en "Invalid argument for option -u: “$SWITCH_UNDECORATED”.\n\tValid arguments:\n\t1  (no borders)\n\t2  (no icons)\n\t3  (no borders, no icons)\n"
					exit 1
				esac
	        ;;	
	        *)  echo "invalid command line argument: -${OPTARG}"
	        exit 1
	        ;;
		esac
	done

if [ $SWITCH_EXPERIENCED_USER == 1 ]; then if [ $SWITCH_FUNCTION != 1 ] && [ $SWITCH_FUNCTION != 2 ]; then echo $"If option -e is set either option -p or -m must be set also." && cleanup && exit $ERR_NO; fi; fi;
if [ $SWITCH_FUNCTION == 1 ] || [ $SWITCH_FUNCTION == 2 ]; then if [ $SWITCH_EXPERIENCED_USER != 1 ]; then echo $"If either option -m or -p is set option -e must be set also." && cleanup && exit $ERR_NO; fi; fi;


function aCMSTS-main-help {
	BUTTONSELECT=255
	if [ $FLAG_STATE != 255 ]; then		# there is a "Back" only possible when called from within program.
		TXT_BUTTON_01=$"Abort"
		TXT_BUTTON_03=$"Back"
		BUTTON_03="--button=$TXT_BUTTON_03:6"
	else
		echo -en $"\n\tHelp not implemented yet\n\n\tin short:\n\t-h\thelp\n\t-e\texperienced user\n\t\t(only in connection with either -m or -p)\n\t-m\tedit menu translations\n\t-p\tedit program translations\n\t\t(-m and -p only in connection with -e).\n"
		echo -en $"\t-u 1\tno window decorations\n\t-u 2\tno icons\n\t-u 3\tneither window decorations nor icons\n\t-d\tprint some debug information when started\n\t\tin terminal window\n\n"	
		FLAG_ABORT=1
		TIMEOUT_10="--timeout=10"
		TXT_BUTTON_01=$"OK"
	fi
	short-help() { 
																								# "--fixed" is necessary here as an aworkaround for a bug in yad causing windows with many text lines to get oversized in heigth, producing endless blank space.
		yad --info $TIMEOUT_10 --width=500 --height=350 --fixed --center $DECOR_ICONS$IMG_ICON_aCMSTS $DECORATIONS \
        --title="$TXT_TITLE" \
        --window-icon="$IMG_ICON" \
        --text=$"\n \n<b>Help not implemented yet.</b>\n\nin short:\n-h\t\thelp\n-e\t\texperienced user\n\t\t(only in connection with either -m or -p)\t\n-m\t\tedit menu translations\n-p\t\tedit program translations\n\t\t(-m and -p only in connection with -e).\t\n-u\t1\tno window decorations.\t\n-u\t2\tno icons\t\n-u\t3\tneither window decorations nor icons\t\n-d\t\tprint some debug information when\t\n\t\tstarted in terminal window\t\n" \
        --button="$TXT_BUTTON_01":4 "$BUTTON_03"
}	
	if [ $FLAG_STATE == 255 ]; then coproc short-help; else short-help; fi

    BUTTONSELECT=$?
	[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4							# catch ESC-Button for exit, refer to man yad for details.
    if [ $BUTTONSELECT == 6 ]; then BUTTON_03="" && return 0; fi				# Back to State where we came from
	if [ $BUTTONSELECT == 4 ]; then FLAG_ABORT=1; fi			# abort
    if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi			# exit processing language file
}

# Selection of functionality
while [ $SWITCH_FUNCTION == 0 ]
do
if [ $SWITCH_EXPERIENCED_USER == 0 ] && [ $SWITCH_FUNCTION != 3 ]; then
			FLAG_STATE=0
			BUTTONSELECT=255
			yad --width=500 --height=650 --fixed --center $DECOR_ICONS$IMG_ICON_aCMSTS $DECORATIONS \
			    --title="$TXT_TITLE" \
			    --window-icon="$IMG_ICON" \
		        --text=$"\n<b>$TXT_TITLE (aCMSTS)\nVersion $VERSION</b>\n \n\tPlease chose which kind of translation file you\n\twould like to access:\n\n\tYou may improve or amend translation strings\n\tin either existing <u><i>program</i></u> translations\n\t(.mo files found in system) or <u><i>menue</i></u>\n\ttranslations (.desktop files found in system).\n\tYou may also add a new translation to any\n\tlanguage known to antiX.\n\n\tPlease consider kindly to transfer your improve-\n\tments to <u>transifex</u> after having successfully\n\ttested them within testing environment provided\n\tby this test suite. By this means you enable\n\tpeople using antiX worldwide stand to benefit\n\tfrom your efforts.\n\n\t<b>Please keep in mind your improvements will\n\tstay permanent on your system (with regard\n\tto updating antiX or the program itself) only\n\twhen applying them to transifex.</b>\n\n\tExperienced users may head for comand-line\n\toptions, saving them from some extra clicks.\n\tBeyond this experienced users may add and\n\ttest translations even to languages not\n\tknown by antiX yet." \
		        --buttons-layout=center \
		        --button=$"Exit":4 --button=$"Menu":6 --button=$"Program":5 --button=$"Help":7
			BUTTONSELECT=$?
			[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4					# catch ESC-Button for exit, refer to man yad for details.

	[ $BUTTONSELECT == 4 ] && cleanup && exit $ERR_NO
	if [ $BUTTONSELECT == 5 ] ;then SWITCH_FUNCTION=1; fi
	if [ $BUTTONSELECT == 6 ] ;then SWITCH_FUNCTION=2; fi
	if [ $BUTTONSELECT == 7 ] ;then aCMSTS-main-help; fi
fi
done

# test whether an instance of this test suite is running already, using lockfile. Multiple instances of this test suite are alowed to run simultaneously, but they cant' share the same working directory, so we have to know.
if ls -l $LOCKFILE* 2>/dev/null 1>&2; then
	FLAG_MULTIPLE=1										# set flag for multiple instances found
	LOCKFILE_CNT="$LOCKFILE"							# each instance has to get it's own dedicated lockfile
	v=0
	while [ -e "$LOCKFILE_CNT" ] 
		do
			v=$(($v+1))
			LOCKFILE_CNT=$(echo "$LOCKFILE"."$v")	
		done
	LOCKFILE="$LOCKFILE_CNT"
	[ $v != 0 ] && TXT_TITLE="$TXT_TITLE [$v]"
fi
>"$LOCKFILE"											# create lockfile

# create Working directory, if not present already. (notice of change: from ver. 0.34 onwards all instances of aCMSTS will use a single working directory)
CURRENT_DATE=`date +%d-%b-%C%y`
v="1"
WORK_DIR_SESSION="$WORK_DIR/$CURRENT_DATE №"
if [ -e "$WORK_DIR_SESSION $v" ] ; then
			while [ -e "$WORK_DIR_SESSION $v" ] 
				do
					v=$(($v+1))
				done
fi
WORK_DIR_SESSION="$WORK_DIR_SESSION $v"
mkdir -p "$WORK_DIR_SESSION"
eval REPORTFILE=$REPORTFILE												# update path to reportfile

# In case multiple instances of antiX multilanguage test suite are running, we can't simply delete virtual antiX language folder for a clear start. So we'll check and increment folder name instead.
if ls -l $LANG_BASE_DIR$antiX_TEST_LANG* 2>/dev/null 1>&2; then
	antiX_TEST_LANG_CNT="$antiX_TEST_LANG"
	v=0
	while [ -e "$LANG_BASE_DIR$antiX_TEST_LANG_CNT" ] 
		do
			v=$(($v+1))
			antiX_TEST_LANG_CNT=$(echo "$antiX_TEST_LANG"."$v")	
		done
	antiX_TEST_LANG="$antiX_TEST_LANG_CNT"								# append incremental counter to all relevant path variables
	antiX_TEST_LANG_DIR="$LANG_BASE_DIR$antiX_TEST_LANG$LANG_MSG_DIR"	
fi

# create list of all available languages antX OS can provide
SYS_LANGUAGES="$(echo -en "$(locale -a | awk '{print substr($0, 1, 5)}' | grep _ | cut -c -2 | awk '!a[$0]++')$(echo "\n")$(locale -a | awk '{print substr($0, 1, 5)}' | grep _ | awk '!a[$0]++')" | sort -u | tr '\n' ',')"
SYS_LANGUAGES=$(echo $SYS_LANGUAGES|cut -c -$((${#SYS_LANGUAGES}-1)))				# delete last seperator
#SYS_LANGUAGES=$(find /usr/share/locale/* -type d | sed 's/^\/usr\/share\/locale\///' | sed 's/\/LC_MESSAGES//' | sort -u | sed 's/.mo$//' | sed ':a;N;$!ba;s/\n/|/g')

# generate debug info
if [ $SWITCH_DEBUG == 1 ]; then
echo "---------------------  Startup section and initial values -------------------"
echo "FLAG_MULTIPLE                       $FLAG_MULTIPLE" 
echo "LOCKFILE                            $LOCKFILE"
echo "TXT_TITLE                           $TXT_TITLE"
echo "IMG_DIR                             $IMG_DIR"
[ ! -e "$IMG_ICON" ] && echo "Icon not found: $IMG_ICON"
[ ! -e "$IMG_ICON_ERROR" ] && echo "Icon (ERROR) not found: $IMG_ICON_ERROR"
[ ! -e "$IMG_ICON_ASK" ] && echo "Icon (ASK) not found: $IMG_ICON_ASK"
[ ! -e "$IMG_ICON_LANG" ] && echo "Icon (LANG) not found: $IMG_ICON_LANG"
[ ! -e "$IMG_ICON_APP" ] && echo "Icon (APP) not found: $IMG_ICON_APP"
[ ! -e "$IMG_ICON_DECOMPILE" ] && echo "Icon (DECOMP.) not found: $IMG_ICON_DECOMPILE"
[ ! -e "$IMG_ICON_STOP" ] && echo "Icon (STOP) not found: $IMG_ICON_STOP"
[ ! -e "$IMG_ICON_TEST" ] && echo "Icon (TEST) not found: $IMG_ICON_TEST"
[ ! -e "$IMG_ICON_SAVE" ] && echo "Icon (SAVE) not found: $IMG_ICON_SAVE"
echo "WORK_DIR                            $WORK_DIR"
echo "WORK_DIR_SESSION:                   $WORK_DIR_SESSION"
fi


# ======================================= MAIN SECTION A - Editing Program translations ==========================================================

if [ $SWITCH_FUNCTION == 1 ]; then
if [ $SWITCH_DEBUG == 1 ]; then echo "Program translation editing"; fi

# Put all language directory names present on the system in a temp file for later use. We'll do it in this place already, before antiX test language folder will be added.
ls -w1 --color=never "$LANG_BASE_DIR" | grep -v "antiX_test" | grep -v "locale.alias" > $TMP_LANG_DIRS	# omit virtual antiX_test language folders if present

# Add symlinks for all files found in corresponding directory of user interface language to antiX test language dir.
# This way the .mo file in question is the only one that will be in foreign language for testing when temporary switching to 
get_permissions
gksudo -- mkdir -p "$antiX_TEST_LANG_DIR"								# create antiX language test directory
[ -e "$LANG_BASE_DIR$antiX_TEST_LANG" ] && FLAG_antiX_LANG_DIR=1		# we need to know on exit whether we have created an own langdir in this instance when running multiple instances of test suite simultaneously
cd "$antiX_TEST_LANG_DIR"												# for creating symlink we need to change to the target directory (see man cp)
gksudo -- cp -s "$UI_LANG_DIR"* .										# create symlinks for standard flavour of user language first
# gksudo -- cp -s "$UI_LANG_DIR_4"* .									# create symlinks for country specific flavour of user language, overwrite.
cd "$WORK_DIR_SESSION"

# generate debug info
if [ $SWITCH_DEBUG == 1 ]; then
echo "---------- Starting workflow to edit program translations ------------"
echo "FLAG_antiX_LANG_DIR                 $FLAG_antiX_LANG_DIR"
echo "antiX_TEST_LANG                     $antiX_TEST_LANG"
echo "LANG_BASE_DIR                       $LANG_BASE_DIR"
echo "LANG_MSG_DIR                        $LANG_MSG_DIR"
echo "antiX_TEST_LANG_DIR                 $antiX_TEST_LANG_DIR"
echo "UI_LANG                             $UI_LANG"
echo "UI_LANG_DIR                         $UI_LANG_DIR"
echo "TMP_LANG_DIRS                       $TMP_LANG_DIRS"
echo "TMP_LANG_FILES                      $TMP_LANG_FILES"
echo "TMP_SH_FILE                         $TMP_SH_FILE"
echo "----------------------------------------------------------------------"
echo " Contents of $antiX_TEST_LANG_DIR:"
ls "$antiX_TEST_LANG_DIR"
echo "----------------------------------------------------------------------"
fi

# create a comma seperated list of all languages on system from temp file
while read -r LANGNAMES
	do
	LANGLIST=$(echo "$LANGLIST$LANGNAMES,")
	done < $TMP_LANG_DIRS 											
LANGLIST=$(echo $LANGLIST|cut -c -$((${#LANGLIST}-1)))
rm $TMP_LANG_DIRS 														# cleanup

# todo: user select working directory. for now we'll use a directory preselected inside script (head).
# todo: user select whether he wants to use system files or previously form tranifex downloaded structure in any other directory.
# todo: make user aware of the existence of more than one folder containing files for the language he has selected (language group, e.g. de, de_CH, de_AT, de_DE)

FLAG_STATE=1
while :; do


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (1) select application
	if [ "$FLAG_STATE" == 1 ]; then
		FLAG_ABORT=0
		reset-workdir
	
		# generate a sorted list of all translations available for all programs, from system language folder
		#LANGFILELIST="$(find /usr/share/locale/*/LC_MESSAGES/*.mo -type f | sed 's/^\/usr\/share\/locale\/.*\/LC_MESSAGES\///' | sort -u | sed ':a;N;$!ba;s/\n/|/g')"
		LANGFILELIST="$(find /usr/share/locale/*/LC_MESSAGES/*.mo -type f | sed 's/^\/usr\/share\/locale\/.*\/LC_MESSAGES\///' | sort -u | sed 's/.mo$//' | sed ':a;N;$!ba;s/\n/|/g')"
		
		BUTTONSELECT=255
		YAD_HEIGHT=175
		TXT_BUTTON_03=$"Add Application to Translations"
		if [ $SWITCH_EXPERIENCED_USER == 1 ]; then						# experienced user will have an aditional choice here to add a first translation to a program ready for translation
			TXT_BODY_EXP=$"\n\tPlease press Button “Add New Application“\n\tin order to manually create a <u>first translation</u>\n\tfor a program accordingly prepared. See\n\thelp for details."
			BUTTON_03="--button=$TXT_BUTTON_03:8"
			YAD_HEIGHT=250
		fi
		# todo: experienced users may add a new application prepared for translation also.
		# dialog for application selection
		SELECTED_APPLICATION="$(yad --width=500 --height=$YAD_HEIGHT --fixed --center $DECOR_ICONS$IMG_ICON_APP $DECORATIONS \
                         --title="$TXT_TITLE" \
                         --window-icon="$IMG_ICON" \
                         --text=$"\n<b>Please select an application from pulldown menu.</b>\n\tOnly Programs with at least one existing\n\ttranslation to any language are listed.$TXT_BODY_EXP\n" \
		                 --form \
		                 --item-separator="|" \
		                 --field=$"Available applications:":CB \
                         "$LANGFILELIST" \
                         --buttons-layout=center \
                         --button=$"Abort":4 "$BUTTON_03" --button=$"Proceed in selection":6)"
		BUTTONSELECT=$?
		[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4			# catch ESC-Button for exit, refer to man yad for details.
	
		SELECTED_APPLICATION=$(echo "$SELECTED_APPLICATION"|cut -d'|' -f1)		  		# set application translation file
		
		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "----------------------------------------------------------"
			echo "FLAG_STATE (1a):        $FLAG_STATE"
			echo "BUTTONSELECT:           $BUTTONSELECT"
			echo "SELECTED_APPLICATION:   $SELECTED_APPLICATION"
		fi
		
		# Logic evaluation
		if [ $BUTTONSELECT == 4 ]; then FLAG_ABORT=1; fi						# abort
		if [ $BUTTONSELECT == 6 ]; then FLAG_NEWLANG=0 && FLAG_STATE=2; fi		# continue, proceed to language selection
		if [ $BUTTONSELECT == 8 ]; then FLAG_STATE=11; fi						# add program (experienced users only)
		
		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "FLAG_STATE (1b):        $FLAG_STATE"
			echo "FLAG_ABORT (1b):        $FLAG_ABORT"
		fi	

	fi
	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi			# exit language file processing at user request		


	# (11) add program
	if [ "$FLAG_STATE" == 11 ]; then
		yad --info --width=500 --height=200 --fixed --center $DECOR_ICONS$IMG_ICON_APP $DECORATIONS \
		--title="$TXT_TITLE" \
		--window-icon="$IMG_ICON" \
		--text="\n<b>Add Program prepared to translate</b>\n\n\tNot implemented yet" \
		--button="Back":5
		FLAG_ABORT=0
		FLAG_STATE=1
	fi
	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi			# exit language file processing at user request		


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (2) language selection
	if [ "$FLAG_STATE" == 2 ]; then
		FLAG_ABORT=0
		reset-workdir
		
		LANGLIST="$(find /usr/share/locale/*/LC_MESSAGES/"$SELECTED_APPLICATION.mo" -type f | sed 's/^\/usr\/share\/locale\///' | sed "s/\/LC_MESSAGES\/$SELECTED_APPLICATION.mo//" | sort -u | sed ':a;N;$!ba;s/\n/|/g')"
		
		BUTTONSELECT=255
		TXT_HEADER=$"\n<b>Please select language of translation to edit\nfrom pulldown menu.</b>"
		TXT_CHOICES=$"\n\n<u>Selected Application:</u>\t“$SELECTED_APPLICATION”\n"
		TXT_BODY=$"\n\tThe list contains only languages to which the\n\tprogram is translated already. Please press\n\tButton “Add Translation“ in order to create\n\ta new translation to a language <u>not in list</u>."
		
		# present user dialog: language selection of file to test as well as application selection, (correlating to readout of FLAG_STATE)
		SELECTED_LANG="$(yad --width=500 --height=265 --fixed --center $DECOR_ICONS$IMG_ICON_LANG $DECORATIONS \
                         --title="$TXT_TITLE" \
                         --window-icon="$IMG_ICON" \
                         --text="$TXT_HEADER$TXT_BODY$TXT_CHOICES" \
		                 --form \
		                 --item-separator="|" \
		                 --field=$"Languages available:":CB \
                         "$LANGLIST" \
                         --buttons-layout=center \
                         --button=$"Abort":4 --button=$"Back to Application selection":5 --button=$"Add new Translation":8 --button=$"Selection done":6)"

		BUTTONSELECT=$?
		[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4			# catch ESC-Button for exit, refer to man yad for details.
		SELECTED_LANG=$(echo "$SELECTED_LANG"|cut -d'|' -f1)          					# set language value
		
		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "----------------------------------------------------------"
			echo "FLAG_STATE (2a):    $FLAG_STATE"
			echo "BUTTONSELECT:       $BUTTONSELECT"
			echo "SELECTED_LANG:      $SELECTED_LANG"
		fi

		# Logic evaluation
		if [ $BUTTONSELECT == 4 ]; then FLAG_ABORT=1; fi				# exit program
		if [ $BUTTONSELECT == 5 ]; then FLAG_STATE=1; fi				# back to application selection
		if [ $BUTTONSELECT == 6 ]; then FLAG_STATE=31; fi				# continue, proceed to decompile and edit file
		if [ $BUTTONSELECT == 8 ]; then FLAG_STATE=21; fi				# add translation to new language from system folders

		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "FLAG_STATE (2b):    $FLAG_STATE"
			echo "FLAG_ABORT (2b):    $FLAG_ABORT"
		fi	
		
		
	fi
	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi						# exit language file processing at user request


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (21) add new language from system range to program
	if [ "$FLAG_STATE" == 21 ]; then
		FLAG_ABORT=0
		
		# construct file and pathnames from gathered informations for later use
		FILENAME_MO=$(echo "$LANG_BASE_DIR$SELECTED_LANG$LANG_MSG_DIR$SELECTED_APPLICATION.mo")		# mo-file including full path

		# create list of all languages in system
		# SYS_LANGUAGE_FOLDERS=$(find /usr/share/locale/* -maxdepth 0 -type d | sed 's/^\/usr\/share\/locale\///' | sed 's/\/LC_MESSAGES//' | sort -u | sed ':a;N;$!ba;s/\n/|/g')

		# create list of all languages in system and differentiate by translations present already for this application
        NEW_LANG_FOLDERS="$(find /usr/share/locale/* -maxdepth 0 -type d | grep -v "antiX_test" | sed 's/^\/usr\/share\/locale\///' | sed 's/\/LC_MESSAGES//' | sort -u)"
        #NEW_LANG_FOLDERS="$(find /usr/share/locale/* -maxdepth 0 -type d | sed 's/^\/usr\/share\/locale\///' | sed 's/\/LC_MESSAGES//' | sort -u)"

		LANGLIST="$LANGLIST|"											# Bugfix: Behaviour of 'while' command in antiX 19.3 is different from antiX 17.4.1

        while read -r -d '|' LANG_EXIST
			do
				NEW_LANG_FOLDERS=$(echo -en "$NEW_LANG_FOLDERS" | grep -vxF "$LANG_EXIST")
			done <<< "$LANGLIST"			
			NEW_LANG_FOLDERS=$(echo "$NEW_LANG_FOLDERS"| sed ':a;N;$!ba;s/\n/|/g')
		
		LANGLIST_01=""
		i=0
		while read -r -d '|' ITEM										# replace linebreaks, format lines
			do
				i=$(($i+1))
				if [ $i == 8 ]; then
					LANGLIST_01="$LANGLIST_01$ITEM\n\t"
					i=0
				else
					LANGLIST_01="$LANGLIST_01$ITEM, "
				fi
			done <<< "$LANGLIST"

		LANGLIST=$(echo $LANGLIST|cut -c -$((${#LANGLIST}-1)))			# Bugfix: Behaviour of 'while' command in antiX 19.3 is different from antiX 17.4.1
				
		BUTTONSELECT=255
		TXT_HEADER=$"\n<b>Add new language</b>\n\tPlease select a language to add to the existing\n\ttranslations of selected application from pulldown\n\tmenu and press “Proceed” to create a new translation\n\tfor the tchosen language."
		TXT_CHOICES=$"\n\n<u>Selected Application:</u>\t“$SELECTED_APPLICATION”"
		TXT_CHOICES_02=$"\n<u>Existing translations:</u>\t$LANGLIST_01"
		TXT_BODY=$"\n\n\tThe pulldown menu contains all languages known to system\n\tby now but omits any language to which the program\n\tis translated yet.\n"
		TXT_FOOT=$"\tThe new file will be derived from the existing language\n\tyou have selected in the step before ($SELECTED_LANG). Please make\n\tsure the translation you have chosen as base <u>was complete</u>.\n"
		TXT_BUTTON_04=$"Generate new Language"
		YAD_HEIGHT=430
		if [ $SWITCH_EXPERIENCED_USER == 1 ]; then 						# experienced user will have an aditional choice here already		
			TXT_BODY_EXP=$"\n\tPlease press Button “Generate New Language“ in order\n\tto manually create a new language for translation not\n\tknown by System yet.\n\n"
			BUTTON_04="--button=$TXT_BUTTON_04:8"
			YAD_HEIGHT=530
		fi
		# dialog for selection of new language for translation of application
		ADDITIONAL_LANG="$(yad --width=500 --height=$YAD_HEIGHT --fixed --center $DECOR_ICONS$IMG_ICON_LANG $DECORATIONS \
                         --title="$TXT_TITLE" \
                         --window-icon="$IMG_ICON" \
                         --text="$TXT_HEADER$TXT_CHOICES$TXT_CHOICES_02$TXT_BODY$TXT_BODY_EXP$TXT_FOOT"\
		                 --form \
		                 --item-separator="|" \
		                 --field=$"New Languages available:":CB \
                         "$NEW_LANG_FOLDERS" \
                         --buttons-layout=center \
                         --button=$"Abort":4 --button=$"Back to Language selection":5 "$BUTTON_04" --button=$"Selection done":6)"

		BUTTONSELECT=$?
		[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4			# catch ESC-Button for exit, refer to man yad for details.
		SELECTED_LANG=$(echo "$ADDITIONAL_LANG"|cut -d'|' -f1)          					# set language value

		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "----------------------------------------------------------"
			echo "FLAG_STATE (21a):    $FLAG_STATE"
			echo "BUTTONSELECT:       $BUTTONSELECT"
			echo "SELECTED_LANG:      $SELECTED_LANG"
			echo -en "SYS_LANGUAGE_FOLDERS:\n $SYS_LANGUAGE_FOLDERS\n\n"
			echo -en "LANGLIST:\n $LANGLIST\n\n"
			echo -en "NEW_LANG_FOLDERS:\n $NEW_LANG_FOLDERS\n"
		fi

		# Logic evaluation
		if [ $BUTTONSELECT == 4 ]; then FLAG_ABORT=1; fi				# exit program
		if [ $BUTTONSELECT == 5 ]; then FLAG_STATE=2; fi				# back to language selection
		if [ $BUTTONSELECT == 6 ]; then FLAG_STATE=32; fi				# create .pot file
		if [ $BUTTONSELECT == 8 ]; then FLAG_STATE=22; fi				# add new language for translation manually

		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "FLAG_STATE (21b):    $FLAG_STATE"
			echo "FLAG_ABORT (21b):    $FLAG_ABORT"
		fi	

	fi
	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi						# exit language file processing at user request


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (22) add new language to program manually (experienced users only)
	if [ "$FLAG_STATE" == 22 ]; then
		FLAG_ABORT=0
		BUTTONSELECT=255
		ADDITIONAL_LANG="$(yad --width=500 --height=300 --fixed --center $DECOR_ICONS$IMG_ICON_LANG $DECORATIONS \
                         --title="$TXT_TITLE" \
                         --window-icon="$IMG_ICON" \
                         --text=$"\n<b>Adding manually new Language unknown\nto system by now.</b>\n\tPlease enter the new Language Identifier.\n\tExamples for valid Identifiers are:\n\n\t\t“fr, pt_BR, de@hebrew,\n\t\ten@cyrillic, zh_CN.GB2312”.\n\n\tYou should be absoulutely sure about what\n\tyou are doing here before proceeding." \
		                 --form \
		                 --item-separator="|" \
		                 --field=$"New Language ID":TEXT \
                         --buttons-layout=center \
                         --button=$"Abort":4 --button=$"Back to Language selection":5 --button=$"Create entered ID":6)"
				
		BUTTONSELECT=$?
		[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4			# catch ESC-Button for exit, refer to man yad for details.
		SELECTED_LANG=$(echo "$ADDITIONAL_LANG"|cut -d'|' -f1)          				# set language value

		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "----------------------------------------------------------"
			echo "FLAG_STATE (22a):    $FLAG_STATE"
			echo "BUTTONSELECT:       $BUTTONSELECT"
			echo "SELECTED_LANG:      $SELECTED_LANG"
		fi
		
		# Logic evaluation
		if [ $BUTTONSELECT == 4 ]; then FLAG_ABORT=1; fi				# exit program
		if [ $BUTTONSELECT == 5 ]; then
			FLAG_STATE=2												# back to language selection
		else
			if [ "$SELECTED_LANG" == "" ]; then FLAG_STATE=22 && BUTTONSELECT=0; fi		# this was no valid input, try again.			
		fi
		if [ $BUTTONSELECT == 6 ]; then									# create .po or .pot file
			FLAG_STATE=32
			if [ -e "$LANG_BASE_DIR$SELECTED_LANG" ]; then
				if [ -e "$LANG_BASE_DIR$SELECTED_LANG$LANG_MSG_DIR$SELECTED_APPLICATION.mo" ]; then
					TXT_BODY=$"\n\tA translation file was found for this language:\n\t$LANG_BASE_DIR$SELECTED_LANG$LANG_MSG_DIR$SELECTED_APPLICATION.mo\n\n\tProceed by editing this file?"
					FLAG_STATE=31										# decompile existing .mo file to .po
				else			
					TXT_BODY=$"\n\tProceed using existing language folder?\n\n\t$LANG_BASE_DIR$SELECTED_LANG\n\n?"
					FLAG_STATE=32										# create new .pot file
				fi									
				BUTTONSELECT=255
				yad --info $TIMEOUT_10 --width=500 --height=200 --fixed --center $DECOR_ICONS$IMG_ICON_ASK $DECORATIONS \
                    --title="$TXT_TITLE" \
                    --window-icon="$IMG_ICON" \
                    --text=$"\n<b>The language ID you‛ve just entered exists already</b>.$TXT_BODY" \
                    --button=$"Abort":4 --button=$"Use existing Language":6

				BUTTONSELECT=$?
				[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4							# catch ESC-Button for exit, refer to man yad for details.
				if [ $BUTTONSELECT == 4 ]; then FLAG_ABORT=1; fi			# abort		

			fi
		fi				
		
		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "SELECTED_LANG:       $SELECTED_LANG"
			echo "FLAG_STATE (22b):    $FLAG_STATE"
			echo "FLAG_ABORT (22b):    $FLAG_ABORT"
		fi	
		
	fi
	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi						# exit language file processing at user request


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (31) decompile .mo file to .po 
	if [ "$FLAG_STATE" == 31 ]; then
		FLAG_ABORT=0
		# decompile .mo file to .po
		get_permissions
		gksudo -- msgunfmt "$(echo "$LANG_BASE_DIR$SELECTED_LANG$LANG_MSG_DIR$SELECTED_APPLICATION.mo")" >"$(echo "$SELECTED_APPLICATION.po")"
		FLAG_STATE=3
		TXT_DLG_MSG=$"Decompiling ˮ.moˮ type “$SELECTED_LANG” language file to editable\nˮ.poˮ type language file."
	fi
	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi					# exit processing language file at user request


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (32) create .pot file from existing .po file
	if [ $FLAG_STATE == 32 ]; then
		FLAG_ABORT=0
		# construct file and pathnames from gathered informations for later use
		LANGFILE_PO=$(echo "$SELECTED_APPLICATION.po")												# po-file without path
		LANGFILE_POT=$(echo "$SELECTED_APPLICATION.pot")											# pot-file without path
		SELECTED_APPLICATION_MO="$SELECTED_APPLICATION.mo"											# mo-file without path
		
		# decompile .mo file to .po
		get_permissions
		gksudo -- msgunfmt "$FILENAME_MO" >"$LANGFILE_PO"

		# remove any translations from .po file and save result to .pot
		touch "$LANGFILE_POT"
		FLAG_MSGID=0
		FLAG_MSGSTR=0
		while read -r "LINE"
			do
				FLAG_COMMENT=0
				if [ -z "$LINE" ]; then FLAG_COMMENT=1 && echo "" >> "$LANGFILE_POT"; fi
				if [ "$(echo "$LINE" | cut -c 1 )" == "#" ]; then FLAG_COMMENT=1 && echo "$LINE" >> "$LANGFILE_POT"; fi
				if ! [ $FLAG_COMMENT == 1 ]; then
					if [ "$(echo "$LINE" | cut -c -5)" == "msgid" ]; then
						FLAG_MSGID=1 && FLAG_MSGSTR=0
					fi
					if [ "$(echo "$LINE" | cut -c -6)"  == "msgstr" ]; then
						FLAG_MSGSTR=1 && FLAG_MSGID=0
						echo -e "msgstr \"\"" >> "$LANGFILE_POT"
					fi
					if [ $FLAG_MSGID == 1 ]; then echo "$LINE" >> "$LANGFILE_POT"; fi
					if [ $FLAG_MSGSTR == 1 ]; then
						[ "$(echo "$LINE" | cut -c 2-14)" == "Content-Type:" ] && echo -E "$LINE" >> "$LANGFILE_POT"
						[ "$(echo "$LINE" | cut -c 2-27)" == "Content-Transfer-Encoding:" ] && echo -E "$LINE" >> "$LANGFILE_POT"
					fi 
				fi
			done < "$LANGFILE_PO"

		# remove sample .po file from working directory before creating language or country specific new .po file
		rm -f "$LANGFILE_PO"
		msginit --input="$LANGFILE_POT" --output="$LANGFILE_PO" --locale=$SELECTED_LANG --no-translator	1>/dev/null 2>&1 # create language specific .po file for new language
		TXT_DLG_MSG=$"Creating empty “.po” type language file prepared for\n“$SELECTED_LANG” translation."
		TXT_REPORT_32=$"An empty “.po” type language file has been created, prepared\nfor “$SELECTED_LANG” translation of “$SELECTED_APPLICATION” program.\n"
		FLAG_STATE=3
	fi


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (3) instruct user where and how to save files correctly as well as correct use of filenames after editing in poedit for further processing 
	if [ "$FLAG_STATE" == 3 ]; then
		FLAG_ABORT=0

		# construct file and pathnames from gathered informations
		FILENAME_MO=$(echo "$LANG_BASE_DIR$SELECTED_LANG$LANG_MSG_DIR$SELECTED_APPLICATION.mo")		# mo-file including full path
		FILENAME_PO=$(echo "$SELECTED_APPLICATION.po")												# po-file without path
		SELECTED_APPLICATION_MO="$SELECTED_APPLICATION.mo"											# mo-file without path
		
		if [ $SWITCH_EXPERIENCED_USER == 0 ]; then							# experienced user should know already what comes next
			
			BUTTONSELECT=255
			TXT_HEADER=$"$TXT_DLG_MSG Please save changes when ready\nbefore closing ˮpoeditˮ, mandatory using filename\n\n\tˮ$FILENAME_POˮ\n\nin the working-directory\n\n\tˮ$WORK_DIR_SESSIONˮ.\n\nPlease make sure the corresponding ˮ.moˮ file\nwill get compiled also.\n"
			TXT_TEXT_02=$"\n\tLanguage: $SELECTED_LANG\n\tApplication: $SELECTED_APPLICATION" 
					
			# dialog to display instructions for mandatory steps user has to perform on his own in poedit
			YAD_OUTPUT="$(yad --width=500 --height=200 --fixed --center $DECOR_ICONS$IMG_ICON_DECOMPILE $DECORATIONS \
		                      --title="$TXT_TITLE" \
		                      --window-icon="$IMG_ICON" \
		                      --text="\n$TXT_HEADER\n$TXT_TEXT_01\n \n"\
                              --buttons-layout=center \
                              --button=$"Exit":4  --button=$"Back to Application Selection":7  --button=$"Back to Language Selection":5  --button=$"Start Editor":6)"
		
			# check user decisions
			BUTTONSELECT=$?
			[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4							# catch ESC-Button for exit, refer to man yad for details.
		else
			if [ $FLAG_NEWLANG == 0 ]; then								# automatic choice for experienced user who doesn't get displayed this dialog
			BUTTONSELECT=6
			else
			BUTTONSELECT=8
			fi
		fi
			
		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "--------------- Decompiling section --------------------------"
			echo "FLAG_STATE (3a):          $FLAG_STATE"
			echo "BUTTONSELECT:             $BUTTONSELECT"
			echo "FILENAME_PO:              $FILENAME_PO"
			echo "FILENAME_MO:              $FILENAME_MO"
			echo "SELECTED_APPLICATION_MO:  $SELECTED_APPLICATION_MO"
		fi
	
		#Logical evaluation
		if [ $BUTTONSELECT == 6 ]; then FLAG_STATE=4; fi						#start poedit
		if [ $BUTTONSELECT == 7 ]; then FLAG_STATE=1 && LANGFILELIST=""; fi		#go back selection of file
		if [ $BUTTONSELECT == 5 ]; then FLAG_STATE=2 && LANGFILELIST=""; fi		#go back selection of language
		if [ $BUTTONSELECT == 4 ]; then FLAG_ABORT=1; fi						#abort
		
		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "FLAG_STATE (3b):         $FLAG_STATE"
			echo "FLAG_ABORT (3b):         $FLAG_ABORT"
		fi
	fi

	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi					# exit processing language file at user request


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (4) edit decompiled .mo file using poedit
	if [ "$FLAG_STATE" == 4 ]; then
		FLAG_ABORT=0

		FILENAME_MO=$(echo "$LANG_BASE_DIR$SELECTED_LANG$LANG_MSG_DIR$SELECTED_APPLICATION.mo")  # selected language may have been changed meanwhile
		
		# since poedit opens all files in separate windows using a single PID only, we have to determine which is which in case there are more instances running, so we cann tell for sure when the one we use here gets closed after user is done with editing.
		LIST_PO_01=$(wmctrl -l |grep "Poedit" | cut -c -10)								# generate list of existing poedit windows
		
		#start poedit with selected and decompiled languagefile
		poedit "$FILENAME_PO" &
		[ $SWITCH_DEBUG == 1 ] && echo "opening file $FILENAME_PO using poedit"
	
		PO_WIN_ID=""
		while [ -z "$PO_WIN_ID" ];
			do
				LIST_PO_02=$(wmctrl -l |grep "Poedit" | cut -c -10)						# get new list of existing poedit windows			
				while read -r PO_INSTANCES												# compare both lists
					do
						TESTSTRING_01=""  												# clear teststring for next pass
						TESTSTRING_01=$(echo "$LIST_PO_01" | grep -F "$PO_INSTANCES")	# put window ID into teststring
						[ -z "$TESTSTRING_01" ] && PO_WIN_ID=$PO_INSTANCES 				# what is _not_ found in first list should be our new poedit window.
					done <<<"$LIST_PO_02"
				[ -z "$PO_WIN_ID" ] && sleep 1											# we have to wait until poedit is started finally and windowmanager gets settled
			done

		wmctrl -i -r $PO_WIN_ID -T "$FILENAME_PO ($SELECTED_LANG) - $TXT_TITLE - Poedit"	# rename window titel
	
		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "--------------- Editing section  --------------------------------------"
			echo "LIST_PO_01                   $LIST_PO_01"
			echo "LIST_PO_02                   $LIST_PO_02"
			echo "PO_WIN_ID                    $PO_WIN_ID"
		fi

		while [ -n "$(wmctrl -l | grep "$PO_WIN_ID")" ] 								# wait before proceeding as long user hasn't closed poedit window.
			do
				sleep 1
			done

		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "poedit was closed by user." # for debug
			echo "------------------------ Testing section -----------------------------"
			echo "Content of folder $WORK_DIR_SESSION :"
			ls ./
			echo "----------------------------------------------------------------------"
		fi

		# Check result of user action after poedit was closed
		if [ -e "$SELECTED_APPLICATION_MO" ]; then													# check whether user has stored file with expected fileame in correct folder
			get_permissions
			gksudo -- unlink "$antiX_TEST_LANG_DIR$SELECTED_APPLICATION_MO"
			gksudo -- cp "$SELECTED_APPLICATION_MO" "$antiX_TEST_LANG_DIR$SELECTED_APPLICATION_MO"	# copy freshly edited .mo file into antiX test language directory for use in runtime test.
			FLAG_STATE=5
		else
			# inform user anticipated file is not present
			BUTTONSELECT=255
			yad --width=500 --height=400 --fixed --center $DECOR_ICONS$IMG_ICON_STOP $DECORATIONS \
		        --title="$TXT_TITLE" \
		        --window-icon="$IMG_ICON" \
                --text=$"\n<b>ERROR</b>: Your compiled languagefile\n\n\t“$SELECTED_APPLICATION_MO”\n\nwas not found in expected folder\n\n\t“$WORK_DIR_SESSION”.\n\nDid you save it using correct\nfolder and filename?\n\nCan‛t proceed.\n\nIn case you just saved your file to a different\nfolder, please press “go back to edit“, reopen\nit in poedit and save it to the correct folder.\nPlease make sure the “.mo” type of language\nfile is created by poedit also." \
                --button=$"Back to edit":5 --button=$"EXIT":7 
			BUTTONSELECT=$?
			[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=7					# catch ESC-Button for exit, refer to man yad for details. 
			if [ $BUTTONSELECT == 5 ]; then FLAG_STATE=4; fi										# go back to edit file again (or just reopen it from elsewhere and save it to the correct place)
			if [ $BUTTONSELECT == 7 ]; then															# Exit
				TXT_REPORT_40=$"The file ˮ$SELECTED_APPLICATION.poˮ ($SELECTED_LANG) in this folder has been decompiled from the file ˮ$SELECTED_APPLICATION_MOˮ in original system folder.\nIt may have been edited with poedit, but was not recompiled to .mo file type before leaving poedit.\n\nOriginal Folder: $LANG_BASE_DIR$SELECTED_LANG$LANG_MSG_DIR"			
				generate-report
				FLAG_ABORT=1
			 fi	
		fi

	fi

	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi					# exit processing language file at user request

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (5) test edited and recompiled .mo file utilising virtual antiX test language in specially prepared terminal window
	if [ "$FLAG_STATE" == 5 ]; then
		FLAG_ABORT=0

		if [ $SWITCH_EXPERIENCED_USER == 0 ]; then
			# inform user how things are done
			#BUTTONSELECT=255
			TXT_TEXT_03=$"\nPlease enter the command to start the application corresponding to\nthe translation you‛ve just edited into the language-file test-tool window\nbesides. You should observe carefully whether all functions in the program\ndo work as expected with your translation file, whether all text strings are\ndisplayed correctly and also whether everything is in its place still. Please\nclose the tool-window to continue after you are done with checking."
			yad --width=700 --height=200 --fixed $DECOR_ICONS$IMG_ICON_TEST $DECORATIONS \
                --timeout=40 \
                --title="$TXT_TITLE" \
                --window-icon="$IMG_ICON" \
                --text="$TXT_TEXT_03" \
                --button=$"Close":8  &
									# leave info box open while test terminal opens
		fi

		# open new terminalwindow set LANGUAGE to "antiX_TEST" for all commands executed in this window
		ROX=0
		if [ ! $(command -v lxterminal) ]; then ROX=1; fi					#antiX 19.x utilises roxterm instead of lxterminal.
		TXT_TITLE_01=$"Test-bench Window ($SELECTED_LANG) - $TXT_TITLE"
		if [ $ROX == 0 ]; then
		# for antiX 17.x utilising LXterminal
			echo -ne "#! /bin/bash\nLANGUAGE="$antiX_TEST_LANG" /bin/bash\n" > "$TMP_SH_FILE"	# workaround since `lxterminal -t "antiX Multilanguage test tool" --command="LANGUAGE="$antiX_TEST_LANG /bin/bash"` wouldn't work for some reason.
			chmod 777 "$TMP_SH_FILE"
			lxterminal -t "$TXT_TITLE_01" --command="/bin/bash '$TMP_SH_FILE'"						# open new terminalwindow set LANGUAGE to "antiX_TEST" for all commands executed in this window
			sleep 3                      														# wait until progID readout is correct
			PROG_ID=$(pgrep -n bash)   															# get ID of most recent instance of bash to gain signal about when user has closed terminalwindow.
			if [ $SWITCH_DEBUG == 1 ]; then
				echo "PROG_ID        $PROG_ID"
				echo "Waiting for user closing Test-Environment"
			fi
			while [ -n "$(ps aux |grep -v grep | grep $PROG_ID)" ] 								# wait before proceeding as long user hasn't closed test window.
				do
					sleep 1
				done
		else
		# for 19.x series: utilising roxterm instead of lxterm	
			echo -ne "#! /bin/bash\nLANGUAGE="$antiX_TEST_LANG" /bin/bash\n" > "$TMP_SH_FILE"	# workaround since `roxterm --title="antiX Multilanguage test tool" --execute="LANGUAGE="$antiX_TEST_LANG /bin/bash"` wouldn't work for some reason.
			sudo -- chmod 777 "$TMP_SH_FILE"
			coproc roxterm -T "$TXT_TITLE_01" --hide-menubar -p "Connectshares" --colour-scheme="default" -e "/bin/bash $TMP_SH_FILE"
			sleep 3                      														# wait until progID readout is correct
			PROG_ID=$(pgrep -n bash)   															# get ID of most recent instance of bash to gain signal about when user has closed terminalwindow.
			if [ $SWITCH_DEBUG == 1 ]; then
				echo "PROG_ID        $PROG_ID"
				echo "Waiting for user closing Test-Environment"
			fi
			while [ -n "$(ps aux |grep -v grep | grep $PROG_ID)" ] 								# wait before proceeding as long user hasn't closed test window.
				do
					sleep 1
				done
		fi
		
		# ask user whether checked .mo file is ready for use, whether it should be copied back into system structure also.
		BUTTONSELECT=255
		TXT_FINAL=$"\nDo you want the .mo-file \n\n\tˮ$SELECTED_APPLICATION_MOˮ ($SELECTED_LANG)\n\nyou‛ve just edited to be copied back to its original place in\nlanguage system folder structure\n\n\tˮ$FILENAME_MOˮ\n\nof the antiX instance just operating this PC?\n\nIn any case you‛ll find all resulting files\nwithin working-directory\n\n\t$WORK_DIR_SESSION\n\nafter leaving."
		TXT_BUTTON_01=$"Back to Edit"
		TXT_BUTTON_02=$"Yes"
		TXT_BUTTON_03=$"No"
		yad --title="$TXT_TITLE" --width=500 --height=200 --fixed --center $DECOR_ICONS$IMG_ICON_SAVE $DECORATIONS \
	        --text="$TXT_FINAL" \
	        --window-icon="$IMG_ICON" \
	        --buttons-layout=center \
	        --button="$TXT_BUTTON_01":4 --button="$TXT_BUTTON_02":5  --button="$TXT_BUTTON_03":6

		BUTTONSELECT="$?"
		[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=6	# catch X and ESC-Button for exit, refer to man yad for details. 
	
	
		if [ $BUTTONSELECT == 4 ]; then			# go back to edit file again
			SWITCH_EXPERIENCED_USER=1
			FLAG_STATE=4
			[ $SWITCH_DEBUG == 1 ] && echo "going back to edit file"
		fi						
		if [ $BUTTONSELECT == 6 ]; then			# We're done. just cleanup left over
			FLAG_ABORT=1
			[ $SWITCH_DEBUG == 1 ] && echo "BUTTONSELECT:          $BUTTONSELECT\nFLAG_ABORT:            $FLAG_ABORT"
			TXT_REPORT_50=$TXT_DBG_NEWLANG$"The file “$SELECTED_APPLICATION_MO” in this folder contains the modified “$SELECTED_LANG” language translation of this file.\nIt has NOT been copied back to its original folder in system.\n\nOriginal Folder: $LANG_BASE_DIR$SELECTED_LANG$LANG_MSG_DIR\n\nThe .po file is the decompilation of the .mo file and directly accessable for further modification in “poedit”.\n"
			generate-report
		fi
		if [ $BUTTONSELECT == 5 ]; then			# copy modified file to language file directory of antiX driving this PC
			FLAG_STATE=6
			TXT_REPORT_60=$TXT_DBG_NEWLANG$"The original file ˮ$SELECTED_APPLICATION_MOˮ ($SELECTED_LANG) has been saved to ˮ$SELECTED_APPLICATION_MO.originalˮ in this folder for backup.\n\nThe file “$SELECTED_APPLICATION_MO” in this folder contains the modified “$SELECTED_LANG” language translation of this file.\n\nOriginal Folder: $LANG_BASE_DIR$SELECTED_LANG$LANG_MSG_DIR\n\nThe modified version has been copied back to original system folder and is in use on the instance of antix running this PC."
			generate-report
		fi

	fi
	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi					# exit processing language file at user request


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (6) copy edited .mo file back to structure before cleanup and leaving
	if [ $FLAG_STATE == 6 ]; then
		# save original mo.file to working directory
		get_permissions
		BACKUP_ORIGINAL_MO="$echo $SELECTED_APPLICATION_MO.original"
		[ -e "$FILENAME_MO" ] && gksudo -- mv "$FILENAME_MO" "$BACKUP_ORIGINAL_MO"
		if ! [ -e "$LANG_BASE_DIR$SELECTED_LANG$LANG_MSG_DIR" ]; then gksudo -- mkdir -p "$LANG_BASE_DIR$SELECTED_LANG$LANG_MSG_DIR";  fi 
		gksudo -- cp "$SELECTED_APPLICATION_MO" "$FILENAME_MO"
		FLAG_ABORT=1
		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "BUTTONSELECT:          $BUTTONSELECT"
			echo "BACKUP_ORIGINAL_MO:    $BACKUP_ORIGINAL_MO"
			echo "saving original .mo file and"
			echo "copying file back to system language structure"
			echo "gksudo -- mv $FILENAME_MO ---> $BACKUP_ORIGINAL_MO"
			echo "gksudo -- cp $SELECTED_APPLICATION_MO ---> $FILENAME_MO"
		fi
	fi
	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi					# exit processing language file at user request



done
fi

# todo: ask user whether he'd like to mark file as checked or tested.
# todo: ask user whether he'd like to upload ready for use file to transifex


# ======================================= MAIN SECTION B - Editing Menu translations ==========================================================
if [ $SWITCH_FUNCTION == 2 ]; then
	if [ $SWITCH_DEBUG == 1 ]; then echo "Menu translation editing"; fi

	FLAG_STATE=2															# start with selecting entry in system standard applications folder
	FLAG_ABORT=0
	FLAG_DONE=0


while :; do
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (1) Folder selection
	if [ "$FLAG_STATE" == 1 ]; then
		BUTTONSELECT=255
		IMAGE_01="$IMG_ICON_LANG"
		TXT_HEADER=$"\n<b>Please select folder from pulldown menu</b>\n"
		TXT_CHOICES=" \n"
		CB_FIELD_01=$"Available Folders:"
		CB_FIELD_01_ENTRIES="$FOLDERLIST"
		TXT_BUTTON_01=$"Abort"
		TXT_BUTTON_02=$"Reset"
		TXT_BUTTON_03=$"Back to Selection"
	fi


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (2) Menu Entry selection
	if [ "$FLAG_STATE" == 2 ]; then
		# Put all .desktop files present in currently chosen applications subdirectory into a temp file
		ls -1 "$DESK_ENTRY_DIR" | grep ".desktop" >"$TMP_LANG_DESK"

		# create a comma seperated list of all .desktop files in current chosen folder from temp file
		while read -r DESKNAMES
			do
			DESKLIST=$(echo "$DESKLIST$DESKNAMES,")
			done < $TMP_LANG_DESK 											
		DESKLIST=$(echo $DESKLIST|cut -c -$((${#DESKLIST}-1)))
		rm "$TMP_LANG_DESK" 										# cleanup
	
		BUTTONSELECT=255
		IMAGE_01="$IMG_ICON_MENU"
		TXT_HEADER=$"\n<b>Please select the “.desktop” entry from pulldown menu</b>\nreferring to the antiX menu entry you want to\ntranslate or correct.\n\n"
		TXT_CHOICES=$"          Selected Directory:    $DESK_ENTRY_DIR\n"
		CB_FIELD_01=$"Available Menue Entries:"
		CB_FIELD_01_ENTRIES="$DESKLIST"
		TXT_BUTTON_01=$"Abort"
		TXT_BUTTON_02=$"Change Directory"
		TXT_BUTTON_03=$"Proceed"
	fi 

	# dialog for folder and menu entry selection
	if [ "$FLAG_STATE" == 1 ] || [ "$FLAG_STATE" == 2 ]; then		
		YAD_OUTPUT="$(yad --width=500 --height=200 --fixed --center $DECOR_ICONS$IMAGE_01 $DECORATIONS \
                         --title="$TXT_TITLE" \
                         --window-icon="$IMG_ICON" \
                         --text="$TXT_HEADER$TXT_CHOICES" \
		                 --form \
		                 --item-separator="," \
		                 --field="$CB_FIELD_01":CB \
                         "$CB_FIELD_01_ENTRIES" \
                         --buttons-layout=center \
                         --button="$TXT_BUTTON_01":4 --button="$TXT_BUTTON_02":5  --button="$TXT_BUTTON_03":6)"

		BUTTONSELECT=$?
		[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4			# catch ESC-Button for exit, refer to man yad for details.
		
		# logic evaluation
		case "$FLAG_STATE" in
			1)	DESK_ENTRY_DIR=$(echo "$YAD_OUTPUT"|cut -d'|' -f1)		# set folder
				[ $BUTTONSELECT == 5 ] && FLAG_STATE=1 					# Reset
				[ $BUTTONSELECT == 4 ] && FLAG_ABORT=1					# Abort (Exit)
				[ $BUTTONSELECT == 6 ] && FLAG_STATE=2 && DESKLIST=""	# back to entry selection
			;;
			2)	SELECTED_DESKTOP_ENTRY=$(echo "$YAD_OUTPUT"|cut -d'|' -f1) # set .desktop entry for edit
				[ $BUTTONSELECT == 5 ] && FLAG_STATE=1					# change directory
				[ $BUTTONSELECT == 4 ] && FLAG_ABORT=1					# Abort (Exit)
				[ $BUTTONSELECT == 6 ] && FLAG_STATE=3					# Proceed to language selection
			;;
		esac

		#Debug mode
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "------------------------------------------------------"
			echo "BUTTONSELECT:   $BUTTONSELECT"
			echo "FLAG_STATE 1,2:   $FLAG_STATE"
			echo "YAD_OUTPUT:     $YAD_OUTPUT"
		fi
	fi

	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi										# exit


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (3) language selection
	if [ "$FLAG_STATE" == 3 ]; then

		# create list of translations found in file
		CHR_0x0A="$(printf '\nx')"; CHR_0x0A=${CHR_0x0A%x}						# create seperator
		FOUND_LANG="$(echo "$(grep "^GenericName" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" | grep -oP '(?<=\[).+?(?=\])')$CHR_0x0A$(grep "^Name" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" | grep -oP '(?<=\[).+?(?=\])')$CHR_0x0A$(grep "^Comment" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" | grep -oP '(?<=\[).+?(?=\])')" | sort -u )"
		i=0
		for ITEM in $FOUND_LANG													# replace linebreaks, format lines
			do
				i=$(($i+1))
				if [ $i == 8 ]; then
					FOUND_LANG_CUT="$FOUND_LANG_CUT$ITEM\n\t"
					i=0
				else
					FOUND_LANG_CUT="$FOUND_LANG_CUT$ITEM, "
				fi
			done
		FOUND_LANG="$(echo "\t$FOUND_LANG_CUT"| sed 's/, $//;s/\\n\\t$//')"		# delete last seperator
		FOUND_LANG_CUT=""

		# get untranslated strings from .desktop entry
		DESK_NAME="$(grep "^Name=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" | cut -d '=' -f 2 )"
		DESK_GENERIC="$(grep "^GenericName=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" | cut -d '=' -f 2 )"
		DESK_COMMENT="$(grep "^Comment=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" | cut -d '=' -f 2 )"

		# get strings for UI language from .desktop entry
		#DESK_NAME_LANG="$(grep "^Name\[$SELECTED_LANG" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" | cut -d '=' -f 2 )"
		#DESK_GENERIC_LANG="$(grep "^GenericName\[$SELECTED_LANG" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" | cut -d '=' -f 2 )"
		#DESK_COMMENT_LANG="$(grep "^Comment\[$SELECTED_LANG" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" | cut -d '=' -f 2 )"

		BUTTONSELECT=255
		TXT_HEADER=$"<b>Select language</b>"
		TXT_TEXT_03=$"\n       Folder: $DESK_ENTRY_DIR\n       Desktop-File:  $SELECTED_DESKTOP_ENTRY" 
		TXT_TEXT_04=$"\nLanguages found in File:\n$FOUND_LANG"
		TXT_TEXT_05=$"\nSelect a <u>language present</u> in Desktop-file\nto edit a translation, or <u>any other language</u>\nto create a new translation."
		CB_FIELD_01=$"Languages:"
		CB_FIELD_01_ENTRIES=$SYS_LANGUAGES
		TXT_BUTTON_03=$"Selection done"
		TXT_BUTTON_02=$"Back to Entry Selection"
		TXT_BUTTON_01=$"Abort"
		IMAGE_01="$IMG_ICON_LANG"

		# dialog for selecting language
        SELECTED_LANG=$(yad --width=500 --height=200 --fixed --center $DECOR_ICONS$IMAGE_01 $DECORATIONS \
		                --title="$TXT_TITLE" \
                        --window-icon="$IMG_ICON" \
                        --text="\n$TXT_HEADER$TXT_TEXT_05\n$TXT_TEXT_03\n$TXT_TEXT_04\n \n"\
                        --form \
		                --item-separator="," \
		                --field="$CB_FIELD_01":CB \
                        "$CB_FIELD_01_ENTRIES" \
                        --buttons-layout=center \
                        --button="$TXT_BUTTON_01":4 --button="$TXT_BUTTON_02":5  --button="$TXT_BUTTON_03":6)
		
		BUTTONSELECT=$?
		[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4							# catch ESC-Button for exit, refer to man yad for details.
		SELECTED_LANG=$(echo "$SELECTED_LANG"|cut -d'|' -f1)

		#Logical evaluation
		if [ $BUTTONSELECT == 6 ]; then FLAG_STATE=4; fi						# start file edit
		if [ $BUTTONSELECT == 5 ]; then FLAG_STATE=2 && DESKLIST=""; fi			# go back to Selection of menu entry 
		if [ $BUTTONSELECT == 4 ]; then FLAG_ABORT=1; fi						# abort

		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "------------------------------------------------------"
			echo "BUTTONSELECT:             $BUTTONSELECT"
			echo "FLAG_STATE (3):           $FLAG_STATE"
			echo "DESK_ENTRY_DIR:           $DESK_ENTRY_DIR"
			echo "SELECTED_DESKTOP_ENTRY:   $SELECTED_DESKTOP_ENTRY"
			echo "SELECTED_LANG:            $SELECTED_LANG"
		fi	

		# get translated strings for chosen language from .desktop entry
		DESK_NAME_LANG="$(grep "^Name\[$SELECTED_LANG" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" | cut -d '=' -f 2 )"
		DESK_GENERIC_LANG="$(grep "^GenericName\[$SELECTED_LANG" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" | cut -d '=' -f 2 )"
		DESK_COMMENT_LANG="$(grep "^Comment\[$SELECTED_LANG" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" | cut -d '=' -f 2 )"
	fi

	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi										# exit



#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (4) editing translated strings
	if [ $FLAG_STATE == 4 ]; then

		BUTTONSELECT=255
		TXT_BUTTON_01=$"Abort"
		TXT_BUTTON_02=$"Back to Language Selection"
		TXT_BUTTON_03=$"Back to Entry Selection"
		TXT_BUTTON_04=$"Test Edits"
		TXT_HEADER=$"\n<b>Add, Delete or Edit Translations of Strings</b>\nAs long as a language entry is empty, the original (untranslated)\nor (if present) the unsiversal translation string for this language\n(two-letter language identifier) will be used instead by antiX\nsystem. You may remove a language entry by emptying\nthe corresponding string."
		TXT_TEXT_03=$"\n\tFolder: $DESK_ENTRY_DIR\n\tDesktop-File:  $SELECTED_DESKTOP_ENTRY\n\tLanguage:  $SELECTED_LANG"
		TXT_TEXT_04=$"\nLanguages found in File:\n$FOUND_LANG"
		TXT_TEXT_05=$"\nOriginal Strings (untranslated):\n\t<u>Name:</u>\t$DESK_NAME\n\t<u>Generic Name</u>:\t$DESK_GENERIC\n\t<u>Comment:</u>\t$DESK_COMMENT\n\nTranslations to “$SELECTED_LANG“ Language:"
		FIELD_01="Name:"
		FIELD_02="Generic Name:"
		FIELD_03="Comment:"
		FIELD_01_ENTRIES="$DESK_NAME_LANG"
		FIELD_02_ENTRIES="$DESK_GENERIC_LANG"
		FIELD_03_ENTRIES="$DESK_COMMENT_LANG"
		IMAGE_01="$IMG_ICON_SAVE"

        # dialog for editing strings
        YAD_OUTPUT="$(yad --center --width=500 --height=200 --fixed $DECOR_ICONS$IMAGE_01 $DECORATIONS \
		                --title="$TXT_TITLE" \
                        --window-icon="$IMG_ICON" \
                        --text="\n$TXT_HEADER\n$TXT_TEXT_03\n$TXT_TEXT_04\n$TXT_TEXT_05\n"\
                        --form \
		                --item-separator="," \
		                --field="$FIELD_01" --field="$FIELD_02" --field="$FIELD_03" \
                        "$FIELD_01_ENTRIES" "$FIELD_02_ENTRIES" "$FIELD_03_ENTRIES" \
                        --buttons-layout=center \
                        --button="$TXT_BUTTON_01":4 --button="$TXT_BUTTON_03":6  --button="$TXT_BUTTON_02":5 --button="$TXT_BUTTON_04":8)"
		
		# check user decisions
		BUTTONSELECT=$?
		[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4							# catch ESC-Button for exit, refer to man yad for details.

		DESK_NAME_LANG_E="$(echo "$YAD_OUTPUT"|cut -d'|' -f1)"
		DESK_GENERIC_LANG_E="$(echo "$YAD_OUTPUT"|cut -d'|' -f2)"
		DESK_COMMENT_LANG_E="$(echo "$YAD_OUTPUT"|cut -d'|' -f3)"

		#Logical evaluation
		if [ $BUTTONSELECT == 8 ]; then FLAG_STATE=5; fi				# Save edits
		if [ $BUTTONSELECT == 5 ]; then FLAG_STATE=3; fi				# go back to language Selection
		if [ $BUTTONSELECT == 6 ]; then FLAG_STATE=2 && DESKLIST=""; fi	# go back to language Selection
		if [ $BUTTONSELECT == 4 ]; then FLAG_ABORT=1; fi				# abort

		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "-------------------------------------------------------"
			echo "BUTTONSELECT:         $BUTTONSELECT"
			echo "FLAG_STATE (4)        $FLAGS_STATE"
			echo -en "YAD_OUTPUT:       $YAD_OUTPUT\n"
			echo "DESK_NAME:            $DESK_NAME"
			echo "DESK_GENERIC:         $DESK_GENERIC"
			echo "DESK_COMMENT:         $DESK_COMMENT"
			echo "DESK_NAME_LANG:       $DESK_NAME_LANG"
			echo "DESK_GENERIC_LANG:    $DESK_GENERIC_LANG"
			echo "DESK_COMMENT_LANG:    $DESK_COMMENT_LANG"
			echo "DESK_NAME_LANG_E:     $DESK_NAME_LANG_E"
			echo "DESK_GENERIC_LANG_E:  $DESK_GENERIC_LANG_E"
			echo "DESK_COMMENT_LANG_E:  $DESK_COMMENT_LANG_E"
			echo "-------------------------------------------------------"
		fi

	fi

	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi			# exit processing language file at user request



#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# (5) writing strings to file 
	if [ $FLAG_STATE == 5 ]; then

		# creating backup copy of original file to working directory
		cp "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY.original"				# backup of original file
		cp "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"						# working copy for recording modified strings
		cp "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" "$TMP_DESKTOP_FILE"											# extra copy for testing new strings in menu
		
		# write Name entry
		FLAG_MODIFIED=0
		if [ "$DESK_NAME_LANG_E" != "$DESK_NAME_LANG" ]; then
			if [ -n "$DESK_NAME_LANG_E" ]; then
				if [ -n "$(grep "^Name\[$SELECTED_LANG\]=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then
					sed -i 's/^Name\['"$SELECTED_LANG"'\]=.*$/Name\['"$SELECTED_LANG"'\]='"$DESK_NAME_LANG_E"'/' "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"							# replace original "Name" language entry in file
					TXT_DBG_NAME=$"The modified “Name” language entry ($SELECTED_LANG) has been replaced in file."
					FLAG_MODIFIED=1
					sed -i '/^Name\[/d;/^Name=/d' "$TMP_DESKTOP_FILE" #prepare menu test
					sed -i '/^\[Desktop Entry\][^\n]*/,$!b;//{x;//p;g};//!H;$!d;x;s//&\nName='"$DESK_NAME_LANG_E"'/' "$TMP_DESKTOP_FILE" #prepare menu test
					[ $SWITCH_DEBUG == 1 ] && echo "N1"
				else
					LASTENTRY="Name"; if [ -z "$(grep "^Name=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then LASTENTRY="\[Desktop Entry\]"; fi;							# make sure new entry is written to the correct position in file
					sed -i '/^'"$LASTENTRY"'[^\n]*/,$!b;//{x;//p;g};//!H;$!d;x;s//&\nName\['"$SELECTED_LANG"'\]='"$DESK_NAME_LANG_E"'/' "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"	# add new entry after last "Name" language entry in file
					TXT_DBG_NAME=$"A new “Name” language entry ($SELECTED_LANG) has been added to file."
					FLAG_MODIFIED=1
					sed -i '/^Name\[/d;/^Name=/d' "$TMP_DESKTOP_FILE" #prepare menu test
					sed -i '/^\[Desktop Entry\][^\n]*/,$!b;//{x;//p;g};//!H;$!d;x;s//&\nName='"$DESK_NAME_LANG_E"'/' "$TMP_DESKTOP_FILE" #prepare menu test
					[ $SWITCH_DEBUG == 1 ] && echo "N2"
				fi
			else
				sed -i '/^Name\['"$SELECTED_LANG"'\]=.*$/d' "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"																				# remove "Name" language entry from file
				TXT_DBG_NAME=$"Existing “Name” language entry ($SELECTED_LANG) has been removed from file"
				FLAG_MODIFIED=1
				sed -i '/^Name\[/d' "$TMP_DESKTOP_FILE" #prepare menu test
				[ $SWITCH_DEBUG == 1 ] && echo "N3"
			fi
		else
			if [ -n "$DESK_NAME_LANG_E" ]; then
				TXT_DBG_NAME=$"The new “Name” language entry ($SELECTED_LANG) was identical to entry found in file. Entry has not been touched."
				[ $SWITCH_DEBUG == 1 ] && echo "N4"
			else
				if [ -n "$(grep "^Name\[$SELECTED_LANG\]=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then
					sed -i '/^Name\['"$SELECTED_LANG"'\]=.*$/d' "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"																			# remove empty "Name" language entry from file
					TXT_DBG_NAME=$"Existing empty “Name” language entry ($SELECTED_LANG) has been removed from file. Shouldn‛t have been there."
					FLAG_MODIFIED=1
					sed -i '/^Name\[/d' "$TMP_DESKTOP_FILE" #prepare menu test
					[ $SWITCH_DEBUG == 1 ] && echo "N5"
				else
					TXT_DBG_NAME=$"No “Name” language entry ($SELECTED_LANG) was found in file, no new entry has been written."
					[ $SWITCH_DEBUG == 1 ] && echo "N6"
				fi
			fi
		fi

		# write GenericName entry
		if [ "$DESK_GENERIC_LANG_E" != "$DESK_GENERIC_LANG" ]; then
			if [ -n "$DESK_GENERIC_LANG_E" ]; then
				if [ -n "$(grep "^GenericName\[$SELECTED_LANG\]=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then
					sed -i 's/^GenericName\['"$SELECTED_LANG"'\]=.*$/GenericName\['"$SELECTED_LANG"'\]='"$DESK_GENERIC_LANG_E"'/' "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"					# replace original "Generic Name" language entry in file
					TXT_DBG_GENERIC=$"The modified “GenericName” language entry ($SELECTED_LANG) has been replaced in file."
					FLAG_MODIFIED=1
					LASTENTRY="Name="; if [ -z "$(grep "^Name=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then LASTENTRY="\[Desktop Entry\]"; fi;									#prepare menu test
					sed -i '/^GenericName\[/d;/^GenericName=/d' "$TMP_DESKTOP_FILE"																										#prepare menu test
					sed -i '/^'"$LASTENTRY"'[^\n]*/,$!b;//{x;//p;g};//!H;$!d;x;s//&\nGenericName='"$DESK_GENERIC_LANG_E"'/' "$TMP_DESKTOP_FILE"											#prepare menu test
					[ $SWITCH_DEBUG == 1 ] && echo "G1"
				else
					LASTENTRY="GenericName"; if [ -z "$(grep "^GenericName=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then LASTENTRY="Name"; if [ -z "$(grep "^Name=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then LASTENTRY="\[Desktop Entry\]"; fi; fi;	# make sure new entry is written to the correct position in file
					sed -i '/^'"$LASTENTRY"'[^\n]*/,$!b;//{x;//p;g};//!H;$!d;x;s//&\nGenericName\['"$SELECTED_LANG"'\]='"$DESK_GENERIC_LANG_E"'/' "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"	# add new entry after last "Generic Name" language entry in file
					TXT_DBG_GENERIC=$"A new “GenericName” language entry ($SELECTED_LANG) has been added to file."
					FLAG_MODIFIED=1
					LASTENTRY="Name="; if [ -z "$(grep "^Name=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then LASTENTRY="\[Desktop Entry\]"; fi;									#prepare menu test
					sed -i '/^GenericName\[/d;/^GenericName=/d' "$TMP_DESKTOP_FILE"																										#prepare menu test
					sed -i '/^'"$LASTENTRY"'[^\n]*/,$!b;//{x;//p;g};//!H;$!d;x;s//&\nGenericName='"$DESK_GENERIC_LANG_E"'/' "$TMP_DESKTOP_FILE"											#prepare menu test
					[ $SWITCH_DEBUG == 1 ] && echo "G2"
				fi
			else
				sed -i '/^GenericName\['"$SELECTED_LANG"'\]=.*$/d' "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"																					# remove "Generic Name" language entry from file
				TXT_DBG_GENERIC=$"Existing “GenericName” language entry ($SELECTED_LANG) has been removed from file"
				FLAG_MODIFIED=1
				sed -i '/^GenericName\[/d' "$TMP_DESKTOP_FILE"																																#prepare menu test
				[ $SWITCH_DEBUG == 1 ] && echo "G3"
			fi
		else
			if [ -n "$DESK_GENERIC_LANG_E" ]; then
				TXT_DBG_GENERIC=$"The new “GenericName” language entry ($SELECTED_LANG) was identical to entry found in file. Entry has not been touched."
				[ $SWITCH_DEBUG == 1 ] && echo "G4"
			else
				if [ -n "$(grep "^GenericName\[$SELECTED_LANG\]=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then
					sed -i '/^GenericName\['"$SELECTED_LANG"'\]=.*$/d' "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"																				# remove empty "Generic Name" language entry from file
					TXT_DBG_GENERIC=$"Existing empty “Generic Name” language entry ($SELECTED_LANG) has been removed from file. Shouldn‛t have been there."
					FLAG_MODIFIED=1
					sed -i '/^GenericName\[/d' "$TMP_DESKTOP_FILE"																															#prepare menu test
					[ $SWITCH_DEBUG == 1 ] && echo "G5"
				else
					TXT_DBG_GENERIC=$"No “GenericName” language entry ($SELECTED_LANG) was found in file, no new entry has been written."
					[ $SWITCH_DEBUG == 1 ] && echo "G6"
				fi
			fi
		fi

		# write Comment entry
		if [ "$DESK_COMMENT_LANG_E" != "$DESK_COMMENT_LANG" ]; then
			if [ -n "$DESK_COMMENT_LANG_E" ]; then
				if [ -n "$(grep "^Comment\[$SELECTED_LANG\]=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then
					sed -i 's/^Comment\['"$SELECTED_LANG"'\]=.*$/Comment\['"$SELECTED_LANG"'\]='"$DESK_COMMENT_LANG_E"'/' "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"							# replace original "Comment" language entry in file
					TXT_DBG_COMMENT=$"The modified “Comment” language entry ($SELECTED_LANG) has been replaced in file."
					FLAG_MODIFIED=1
					LASTENTRY="GenericName="; if [ -z "$(grep "^GenericName=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then LASTENTRY="Name="; if [ -z "$(grep "^Name=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then LASTENTRY="\[Desktop Entry\]"; fi; fi; #prepare menu test
					sed -i '/^Comment\[/d;/^Comment=/d' "$TMP_DESKTOP_FILE"																												# prepare menu test
					sed -i '/^'"$LASTENTRY"'[^\n]*/,$!b;//{x;//p;g};//!H;$!d;x;s//&\nComment='"$DESK_COMMENT_LANG_E"'/' "$TMP_DESKTOP_FILE"												# prepare menu test
					[ $SWITCH_DEBUG == 1 ] && echo "C1"
				else
					LASTENRY="Comment"; if [ -z "$(grep "^Comment=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then LASTENTRY="GenericName"; if [ -z "$(grep "^GenericName=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then LASTENTRY="Name"; if [ -z "$(grep "^Name=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then LASTENTRY="\[Desktop Entry\]"; fi; fi; fi;	# make sure new entry is written to the correct position in file
					sed -i '/^'"$LASTENTRY"'[^\n]*/,$!b;//{x;//p;g};//!H;$!d;x;s//&\nComment\['"$SELECTED_LANG"'\]='"$DESK_COMMENT_LANG_E"'/' "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"		# add new entry after last "Comment" language entry in file
					TXT_DBG_COMMENT=$"A new “Comment” language entry ($SELECTED_LANG) has been added to file."
					FLAG_MODIFIED=1
					LASTENTRY="GenericName="; if [ -z "$(grep "^GenericName=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then LASTENTRY="Name="; if [ -z "$(grep "^Name=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then LASTENTRY="\[Desktop Entry\]"; fi; fi; #prepare menu test
					sed -i '/^Comment\[/d;/^Comment=/d' "$TMP_DESKTOP_FILE"																												# prepare menu test
					sed -i '/^'"$LASTENTRY"'[^\n]*/,$!b;//{x;//p;g};//!H;$!d;x;s//&\nComment='"$DESK_COMMENT_LANG_E"'/' "$TMP_DESKTOP_FILE"												# prepare menu test
					[ $SWITCH_DEBUG == 1 ] && echo "C2"
				fi
			else
				sed -i '/^Comment\['"$SELECTED_LANG"'\]=.*$/d' "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"																						#  remove "Comment" language entry from file
				TXT_DBG_COMMENT=$"Existing “Comment” language entry ($SELECTED_LANG) has been removed from file"
				FLAG_MODIFIED=1
				sed -i '/^Comment\[/d' "$TMP_DESKTOP_FILE"																																	#prepare menu test
				[ $SWITCH_DEBUG == 1 ] && echo "C3"
			fi
		else
			if [ -n "$DESK_COMMENT_LANG_E" ]; then
				TXT_DBG_COMMENT=$"The new “Comment” language entry ($SELECTED_LANG) was identical to entry found in file. Entry has not been touched."
				[ $SWITCH_DEBUG == 1 ] && echo "C4"
			else
				if [ -n "$(grep "^Comment\[$SELECTED_LANG\]=" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY")" ]; then
					sed -i '/^Comment\['"$SELECTED_LANG"'\]=.*$/d' "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"																					# remove empty "Comment" language entry from file
					TXT_DBG_COMMENT=$"Existing empty “Comment” language entry ($SELECTED_LANG) has been removed from file. Shouldn‛t have been there."
					FLAG_MODIFIED=1
					sed -i '/^Comment\[/d' "$TMP_DESKTOP_FILE"																																#prepare menu test
					[ $SWITCH_DEBUG == 1 ] && echo "C5"
				else
					TXT_DBG_COMMENT=$"No “Comment” language entry ($SELECTED_LANG) was found in file, no new entry has been written."
					[ $SWITCH_DEBUG == 1 ] && echo "C6"
				fi
			fi
		fi

		# generate debug info
		if [ $SWITCH_DEBUG == 1 ]; then
			echo "-------------------------------------------------------"
			echo "Content of modified .desktop file ($SELECTED_DESKTOP_ENTRY):"
			cat "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY"
			echo "-------------------------------------------------------"
			echo "Content of temporary .desktop file prepared for menu testing:"
			cat "$TMP_DESKTOP_FILE"
			echo "-------------------------------------------------------"
			echo "FLAG_MODIFIED:          $FLAG_MODIFIED"
			echo "LASTENTRY:              $LASTENTRY"		
			echo "Results (Summary)"
			echo "     $TXT_DBG_NAME"
			echo "     $TXT_DBG_GENERIC"
			echo "     $TXT_DBG_COMMENT"
		fi

		# test modified .desktop file entry using menu
		if [ $FLAG_MODIFIED == 1 ]; then
			get_permissions			
			echo -en $"$TXT_TITLE, Version $VERSION\n\nThe original file “$SELECTED_DESKTOP_ENTRY” has been saved to “$SELECTED_DESKTOP_ENTRY.original” in this folder for backup.\nThe file “$SELECTED_DESKTOP_ENTRY” in this folder contains following modifications:\n\n\t1.) $TXT_DBG_NAME\n\n\t2.) $TXT_DBG_GENERIC\n\n\t3.) $TXT_DBG_COMMENT\n\nOriginal Folder: $DESK_ENTRY_DIR\n" >"$WORK_DIR_SESSION/aCMSTS-report.txt"
			gksudo -- cp -f "$TMP_DESKTOP_FILE" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY"									# copy prepared file for testing to system folder
			LANGUAGE=$LANG gksudo -- /usr/local/lib/desktop-menu/desktop-menu-apt-update force	2>/dev/null 1>&2							# activate changes in menu for test
			gksudo -- cp -f "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY.original" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY"	# restore original file (let's do this here already precautionarily, in case of abnormal program termination)

			BUTTONSELECT=255
			TXT_HEADER=$"<b>Check edits in menu</b>\nYou will find your modifications in antiX menu now.\n<u>While keeping this window open</u>, please check whether they meet the requirements,\nand decide if you want to keep, dismiss, or re-edit the translation of the menu entry.\n"
			TXT_TEXT_02=$"\nThe modified entry is overlayed to the menue, irrespective of its language. The entry\nwill be visable off this test suite only when antiX system is localised accordingly.\nPlease be aware of a <u>possibly new sorting</u> of this entry in menu, depending\non its new first letter. In any case you will find all files in working directory\n\n\t“$WORK_DIR_SESSION”\n\nafter leaving.\n" 
			TXT_BUTTON_03=$"Keep Changes, Exit"
			TXT_BUTTON_02=$"Restore Original Version, Exit"
			TXT_BUTTON_01=$"Back to Edit"
			IMAGE_01="$IMG_ICON_TEST"

			# dialog: Keep modified, restore original or go back to edit
			yad --width=500 --height=200 --fixed --center $DECOR_ICONS$IMAGE_01 $DECORATIONS \
		                    --title="$TXT_TITLE" \
		                    --window-icon="$IMG_ICON" \
		                    --text="\n$TXT_HEADER$TXT_TEXT_02\n \n"\
                            --buttons-layout=center \
                            --button="$TXT_BUTTON_01":4 --button="$TXT_BUTTON_02":5  --button="$TXT_BUTTON_03":6
			BUTTONSELECT=$?
			[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4							# catch ESC-Button for exit, refer to man yad for details.

			# generate debug info
			if [ $SWITCH_DEBUG == 1 ]; then
				echo "-------------------------------------------------------"
				echo "BUTTONSELECT:         $BUTTONSELECT"
			fi

			# logic evaluation
			if [ $BUTTONSELECT == 5 ]; then FLAG_ABORT=1; fi				# Restore original
			if [ $BUTTONSELECT == 4 ]; then FLAG_STATE=4; fi				# Back to edit strings
			if [ $BUTTONSELECT == 6 ]; then FLAG_STATE=6; fi				# Keep modifications and Exit
		
			[ $BUTTONSELECT == 5 ] && echo -en $"The language modified version of this file was NOT activated on the antiX instance just operating this PC.\n" >>"$WORK_DIR_SESSION/aCMSTS-report.txt"
			[ $BUTTONSELECT == 6 ] && echo -en $"The language modified version of this file is in use on the antiX instance just operating this PC.\n" >>"$WORK_DIR_SESSION/aCMSTS-report.txt"
			echo -en "Date of modification: `date`\n" >>"$WORK_DIR_SESSION/aCMSTS-report.txt"
		
		else	# no differences between translated entries in file and new strings
			BUTTONSELECT=255
			TXT_HEADER=$"\nNo modifications have been made to original translation.\nDo you want to edit Entry again?"
			TXT_TEXT_02=$"\nIn any case you will find all files in working directory\n\t$WORK_DIR_SESSION\nafter leaving.\n" 
			TXT_BUTTON_02=$"Exit"
			TXT_BUTTON_01=$"Back to Edit"
			IMAGE_01="$IMG_ICON_ASK"
			# dialog: exit or back to edit
			yad --width=500 --height=200 --fixed --center $DECOR_ICONS$IMAGE_01 $DECORATIONS \
		                    --title="$TXT_TITLE" \
		                    --window-icon="$IMG_ICON" \
		                    --text="\n$TXT_HEADER\n$TXT_TEXT_01\n \n"\
                            --buttons-layout=center \
                            --button="$TXT_BUTTON_01":4 --button="$TXT_BUTTON_02":5
		
			# check user decisions
			BUTTONSELECT=$?
			[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4							# catch ESC-Button for exit, refer to man yad for details.

			# generate debug info
			if [ $SWITCH_DEBUG == 1 ]; then
				echo "-------------------------------------------------------"
				echo "BUTTONSELECT:         $BUTTONSELECT"
			fi

			# logic evaluation
			if [ $BUTTONSELECT == 4 ]; then FLAG_STATE=4; fi				# Back to edit strings
			if [ $BUTTONSELECT == 5 ]; then FLAG_STATE=6; fi				# Keep modifications and Exit
	
		fi	

		if [ $FLAG_STATE != 6 ]; then										# activate restored original menu
			get_permissions
			LANGUAGE=$LANG gksudo -- /usr/local/lib/desktop-menu/desktop-menu-apt-update force 2>/dev/null 1>&2
		fi

	fi

	
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	# (6) replace original file by modified copy
	if [ $FLAG_STATE == 6 ]; then
		#copy modified file back to system and activate changes in menu.
		get_permissions
		gksudo -- cp -f "$WORK_DIR_SESSION/$SELECTED_DESKTOP_ENTRY" "$DESK_ENTRY_DIR/$SELECTED_DESKTOP_ENTRY" 		# copy modified file for use to system folder
		LANGUAGE=$LANG gksudo -- /usr/local/lib/desktop-menu/desktop-menu-apt-update force 2>/dev/null 1>&2						# activate modifications in menu
		FLAG_ABORT=1
	fi
	
	if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi			# exit processing language file



done
fi


# ============================== MAIN SECTION C - Help to translation methods for menu and program files ======================================
if [ $SWITCH_FUNCTION == 3 ]; then
aCMSTS-main-help
fi

#if [ $SWITCH_FUNCTION == 3 ]; then	
#	BUTTONSELECT=255
#	if [ $FLAG_STATE != 255 ]; then		# there is a "Back" only possible when called from within program.
#		TXT_BUTTON_01=$"Abort"
#		TXT_BUTTON_03=$"Back"
#		BUTTON_03="--button=$TXT_BUTTON_03:6"
#	else
#		echo -en $"\n\tHelp not implemented yet\n\n\tin short:\n\t-h\thelp\n\t-e\texperienced user\n\t\t(only in connection with either -m or -p)\n\t-m\tedit menu translations\n\t-p\tedit program translations\n\t\t(-m and -p only in connection with -e).\n"
#		echo -en $"\t-u 1\tno window decorations\n\t-u 2\tno icons\n\t-u 3\tneither window decorations nor icons\n\t-d\tprint some debug information when started\n\t\tin terminal window\n\n"	
#		FLAG_ABORT=1
#		TIMEOUT_10="--timeout=10"
#		TXT_BUTTON_01=$"OK"
#	fi
#
#	yad --info $TIMEOUT_10 --center $DECOR_ICONS$IMG_ICON_aCMSTS $DECORATIONS \
#       --title="$TXT_TITLE" \
#       --window-icon="$IMG_ICON" \
#       --text=$"\n \n\tHelp not implemented yet.\n\n\tin short:\n\t-h\thelp\n\t-e\texperienced user\n\t \t(only in connection with either -m or -p)\t\n\t-m\tedit menu translations\n\t-p\tedit program translations\n\t \t(-m and -p only in connection with -e).\t\n\t-u\t1  no window decorations.\t\n\t-u\t2  no icons\t\n\t-u\t3  neither window decorations nor icons\t\n\t-d\tprint some debug information when\t\n\t \tstarted in terminal window\t" \
#       --button="$TXT_BUTTON_01":4 "$BUTTON_03"
#
#    BUTTONSELECT=$?
#	[ $BUTTONSELECT == 252 ] || [ $BUTTONSELECT == 255 ] && BUTTONSELECT=4							# catch ESC-Button for exit, refer to man yad for details.
#    if [ $BUTTONSELECT == 6 ]; then return 0; fi				# Back to State where we came from
#	if [ $BUTTONSELECT == 4 ]; then FLAG_ABORT=1; fi			# abort
#    
#    if [ $FLAG_ABORT == 1 ]; then cleanup && exit $ERR_NO; fi			# exit processing language file
#	
#    
#fi
# ==============================================================================================================================================



#    License Information for:
#    antiX Community Multilanguage Support Test Suite
#    (Please don't remove. You may simply add licence information
#    concerning your contributions above or below.)
#
#    antiX Community Multilanguage Test Suite (aCMTS) provides an easy
#    way of handling .mo files for foreign language support in antiX.
#    It integrates downloading from transifex or selecting from system-
#    textdomain folders, decompiling, editing, recompiling and saving
#    to the correct place in user system as well as testing in runtime
#    and re-up loading transifex again. The structure engages different
#    existing programs (e.g. poedit) and manages the interaction between
#    them and the user. It is meant for use of unexpereinced users in
#    the first place, who do want to help in translation and testing
#    functionality of translations of any programs included in antiX.
#    The corresponding translations of Menu entries can get edited and
#    tested also.   
#
#    Copyright 2020,2021 Robin.antiX
#    "This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>."

#    All icons used were either found under CC 4.0 international license
#    https://creativecommons.org/licenses/by-sa/4.0/deed.en
#    or are under CC0 or are otherwise in public domain.
#    they originate from
#    https://commons.wikimedia.org/wiki/File:Article-improve-translation.svg
#    https://commons.wikimedia.org/wiki/File:Desc-i.svg
#    https://commons.wikimedia.org/wiki/File:GeoBot_Logo.svg
#    https://commons.wikimedia.org/wiki/File:Globelang.svg
#    https://commons.wikimedia.org/wiki/File:Globe_of_letters.png
#    https://commons.wikimedia.org/wiki/File:GDJ-World-Flags-Globe-With-Shading.svg
#    https://commons.wikimedia.org/wiki/File:Multilingual_support_symbol.svg
#    https://commons.wikimedia.org/wiki/File:Chart_of_world_writing_systems.svg
#    https://commons.wikimedia.org/wiki/File:Gnome-dialog-warning2.svg
#    https://commons.wikimedia.org/wiki/File:Gnome-document-save.svg
#    https://commons.wikimedia.org/wiki/File:Gnome-dialog-question.svg
#    https://commons.wikimedia.org/wiki/File:Gnome-applications-other.svg
