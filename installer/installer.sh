#! /bin/bash
# installer script for antiX Community Multilanguage Support Test Suite (aCMSTS) v.0.37
# written for antiX Community by Robin.antiX Jan-Feb.2021
# Please respect licence information at bottom of this script.

ARCHIVE_FILE="./aCMSTS.tar"
RES_DIR="./ressources"
IMG_SRC="$RES_DIR/icons"
LOCALE_SRC="$RES_DIR/locale"

IMG_DIR="/usr/share/icons/hicolor/scalable/apps"
LOCALE_DIR="/usr/share/locale"
PROG_DIR="/usr/local/bin"
DESKTOP_APPS_DIR="/usr/share/applications"

FLAG_ERR=0

echo -en "\nInstaller for ˮantiX Community Multilanguage Support Test Suite (aCMSTS)ˮ\n"
[ ! -e "./installer.sh" ] && echo -en "\n   This is a really simple installer only. Hence,\n   please 'cd' to the directory this installer script\n   is residing in and start it from there.\n\nLeaving.\n" && exit 1
[ ! -e "$ARCHIVE_FILE" ] && echo -en "\n   The archive \"aCMSTS.tar\" was not found! Can't proceed.\n   Please make sure this archive resides in the same directory\n   as this installer.\n\nLeaving.\n" && exit 1
#echo -en "\n\n"


# get permission from user for writing in system folders
gksudo -- whoami > /dev/null
[ $? != 0 ] && echo -en "\nCan't proceed due to lack of permissions\n ...leaving.\n" && exit 1

# extract archive file
echo -en "\nExtracting archive...\n"
tar -xf "$ARCHIVE_FILE"
[ ! -e "$RES_DIR" ] && echo -en "\n   The folder \"ressources\" was not found! Can't proceed.\n   Please make sure this folder resides in the same directory\n   as this installer and contains all subdirectories and\n   files from the archive file.\n\nLeaving.\n" && exit 1
echo -en "   ... done\n"

# check whether files didn't get damaged on transport
echo -en "\nChecking files...\n"
shasum -a 256 -c "./ressources/ressources-sha256.sum"
[ $? != 0 ] && echo -en "\nCan't proceed: Checksums do not match.\nArchived files seem to be damaged.\n ...leaving.\n" && exit 1
echo -en "   ... done\n"

# copy icons to its place in folderstructure
echo -en "\nStarting to copy files...\n"
[ ! -e "$IMG_DIR" ] && gksudo mkdir -p "$IMG_DIR"
NEW_ICONS="$(ls -1 "$IMG_SRC")"
		while read -r ENTRY_ICON
			do
				echo "copying $ENTRY_ICON to $IMG_DIR"
				gksudo -- cp -f "$IMG_SRC/$ENTRY_ICON" "$IMG_DIR"
				gksudo -- chown root:root "$IMG_DIR/$ENTRY_ICON"
				gksudo -- chmod 644 "$IMG_DIR/$ENTRY_ICON"
				if [ -e "$IMG_DIR/$ENTRY_ICON" ]; then echo -en "   ... done\n"; else FLAG_ERR=1 && echo -en "   ERROR: copying file has failed\n"; fi
			done <<<"$NEW_ICONS"

# copy all locales found in subdirectory to their place in folderstructure
NEW_LOCALE=$(ls -1 "$LOCALE_SRC")
while read -r ENTRY_LANGUAGE
	do
		[ ! -e "$LOCALE_DIR/$ENTRY_LANGUAGE/LC_MESSAGES" ] && gksudo -- mkdir -p "$LOCALE_DIR/$ENTRY_LANGUAGE/LC_MESSAGES"
		NEW_FILES="$(ls -1 "$LOCALE_SRC/$ENTRY_LANGUAGE/LC_MESSAGES")"
		while read -r ENTRY_FILE
			do
				echo "copying $ENTRY_FILE to $LOCALE_DIR/$ENTRY_LANGUAGE/LC_MESSAGES"
				gksudo -- cp -f "$LOCALE_SRC/$ENTRY_LANGUAGE/LC_MESSAGES/$ENTRY_FILE" "$LOCALE_DIR/$ENTRY_LANGUAGE/LC_MESSAGES"
				gksudo -- chown root:root "$LOCALE_DIR/$ENTRY_LANGUAGE/LC_MESSAGES/$ENTRY_FILE"
				gksudo -- chmod 644 "$LOCALE_DIR/$ENTRY_LANGUAGE/LC_MESSAGES/$ENTRY_FILE"
				if [ -e "$LOCALE_DIR/$ENTRY_LANGUAGE/LC_MESSAGES/$ENTRY_FILE" ]; then echo -en "   ... done\n"; else FLAG_ERR=1 && echo -en "\n   ERROR: copying file has failed\n"; fi
			done <<<"$NEW_FILES"
	done <<<"$NEW_LOCALE"

# copy script in its place
echo "Copying antiX-multilanguage-test-suite.sh to $PROG_DIR"
gksudo -- cp -f "$RES_DIR/antiX-multilanguage-test-suite.sh" "$PROG_DIR"
gksudo -- chown root:root "$PROG_DIR/antiX-multilanguage-test-suite.sh"
gksudo -- chmod 755 "$PROG_DIR/antiX-multilanguage-test-suite.sh"
if [ -e "$PROG_DIR/antiX-multilanguage-test-suite.sh" ]; then echo -en "   ... done\n"; else FLAG_ERR=1 && echo -en "\n   ERROR: copying file has failed\n"; fi

# copy desktop file to its place in folderstructure
echo "Copying aCMSTS.desktop to $DESKTOP_APPS_DIR"
gksudo -- cp -f "$RES_DIR/aCMSTS.desktop" "$DESKTOP_APPS_DIR"
gksudo -- chown root:root "$DESKTOP_APPS_DIR/aCMSTS.desktop"
gksudo -- chmod 644 "$DESKTOP_APPS_DIR/aCMSTS.desktop"
if [ -e "$DESKTOP_APPS_DIR/aCMSTS.desktop" ]; then echo -en "   ... done\n"; else FLAG_ERR=1 && echo -en "\n   ERROR: copying file has failed\n"; fi

# update main menu with new entry
echo -en "\nUpdating main menu...\n"
gksudo -- /usr/local/lib/desktop-menu/desktop-menu-apt-update force

if [ "$FLAG_ERR" == 0  ]; then echo -en "\n\nInstallation has completed successfully.\n\n   You will find ˮantiX Community Multilanguage Support Test Suite (aCMSTS)ˮ\n   in main menue --> programs --> development.\n\n"; else echo -en "\n\n   Installation failed.\n\n   Please check manually whether all files are in place.\n\n"  ; fi

if [ ! $(command -v poedit) ]; then echo -en "\n   The required program ˮpoeditˮ is not installed on this system.\n   --> Please make sure ˮpoeditˮ is installed before starting\n      ˮantiX Community Multilanguage Support Test Suite (aCMSTS)ˮ\n" && FLAG_MISSING=1; fi
if [ ! $(command -v msgunfmt) ]; then echo -en "\n   The command ˮmsgunfmtˮ is not found on this system.\n   --> Please make sure ˮgettextˮ is installed before starting\n      ˮantiX Community Multilanguage Support Test Suite (aCMSTS)ˮ.\n" && FLAG_MISSING=1; fi


exit 0


#    License Information for:
#    antiX Community Multilanguage Support Test Suite installer script
#    (Please don't remove. You may simply add licence information
#    concerning your contributions above or below.)
#
#    antiX Community Multilanguage Test Suite (aCMTS) provides an easy
#    way of handling .mo files for foreign language support in antiX.
#    It integrates downloading from transifex or selecting from system-
#    textdomain folders, decompiling, editing, recompiling and saving
#    to the correct place in user system as well as testing in runtime
#    and re-up loading transifex again. The structure engages different
#    existing programs (e.g. poedit) and manages the interaction between
#    them and the user. It is meant for use of unexpereinced users in
#    the first place, who do want to help in translation and testing
#    functionality of translations of any programs included in antiX.
#   
#    Copyright 2020,2021 Robin.antiX
#    "This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>."
